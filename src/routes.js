import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom' // eslint-disable-line 
import Loadable from 'react-loadable'
import { Loading } from './components/common' 
import NoMatch from './components/common/NoMatch/index'

const Home = Loadable({ loader: () => import('./components/Home'), loading: Loading })
const Events = Loadable({ loader: () => import('./components/Events'), loading: Loading })
const Gallery = Loadable({ loader: () => import('./components/Gallery'), loading: Loading })
const Bar = Loadable({ loader: () => import('./components/Bar'), loading: Loading })
const Masters = Loadable({ loader: () => import('./components/Masters'), loading: Loading })
const TimeTable = Loadable({ loader: () => import('./components/TimeTable'), loading: Loading })
const Price = Loadable({ loader: () => import('./components/Price'), loading: Loading })
const Pool = Loadable({ loader: () => import('./components/Pool'), loading: Loading })
const Person = Loadable({ loader: () => import('./components/Person'), loading: Loading })
const CheckToken = Loadable({ loader: () => import('./components/Person/CheckToken'), loading: Loading })
const CheckToPay = Loadable({ loader: () => import('./components/Price/CheckToPay'), loading: Loading })
const Contact = Loadable({ loader: () => import('./components/Contacts'), loading: Loading })
const Travels = Loadable({ loader: () => import('./components/Travels'), loading: Loading })
const Reserve = Loadable({ loader: () => import('./components/Person/Reserve'), loading: Loading })
const Success = Loadable({ loader: () => import('./components/common/Success'), loading: Loading })
const Error = Loadable({ loader: () => import('./components/common/Error'), loading: Loading })
const Privacy = Loadable({ loader: () => import('./components/Privacy/index'), loading: Loading })

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/events" component={Events} />
    <Route path="/gallery" component={Gallery} />
    <Route path="/bar" component={Bar} />
    <Route path="/master" component={Masters} />
    <Route path="/timetable" component={TimeTable} />
    <Route path="/price" component={Price} />
    <Route path="/pool" component={Pool} />
    <Route path="/travels" component={Travels} />
    <Route path="/person" component={Person} />
    <Route path="/checktoken" component={CheckToken} />
    <Route path="/checktopay" component={CheckToPay} />
    <Route path="/contacts" component={Contact} />
    <Route path="/loading" component={Loading} />
    <Route path="/reserve" component={Reserve} />
    <Route path="/success" component={Success} />
    <Route path="/error" component={Error} />
    <Route path="/privacy" component={Privacy} />
    <Route component={NoMatch} />
  </Switch>
)

export default Routes
