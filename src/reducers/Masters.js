const INITIAL_STATE = {
  masters: [],
  master: {},
  localmaster: {},
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case 'MASTERS_FETCHED': 
    return {
      ...state, masters: action.payload, loading: true
    }
  case 'MASTER_FETCHED': 
    return {
      ...state, 
      master: action.payload.success, 
      localmaster: action.payload.newData,
      loading: true 
    }
  case 'LOGOUT':
    return INITIAL_STATE 

  default: 
    return state
  }
} 
