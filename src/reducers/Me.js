const INITIAL_STATE = {
  info: {},
  clientData: {},
  historyData: [],
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case 'GET_ME_INFO':
    return {
      ...state,
      info: action.info,
      loading: true
    }
  case 'GET_ME_INFO_ERROR':
    return {
      ...state
    }
  case 'CLIENT_FETCHED':
    return {
      ...state, 
      clientData: action.payload,
      loading: true
    }
  case 'CLIENT_FETCHED_ERROR':
    return {
      ...state
    }
  case 'GET_CLIENT_VISIT':
    return {
      ...state, 
      historyData: action.payload,
      loading: true
    }
  case 'ERROR_CLIENT_VISITED':
    return {
      ...state
    }
  case 'LOGOUT':
    return INITIAL_STATE 
  default:
    return state
  }
}
