const INITIAL_STATE = {
  shedule: [],
  item: {
    AppointmentID: '',
    Employee: {
      FullName: ''
    },
    StartDate: '',
    EndDate: '',
    Room: {
      Title: ''
    },
    Service: {
      Title: '',
      Description: ''
    },
    Duration: ''
  },
  Reserved: false,
  card: true,
  loading: false 
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case 'SPINNER': 
    return {
      ...state, 
      loading: action.payload 
    }
  case 'SCHEDULE_FETCHED': 
    return {
      ...state, 
      shedule: action.payload,
      loading: true
    }
  case 'ERROR_SCHEDULE_FETCHED': 
    return {
      ...state 
    }
  case 'SCHEDULE_ITEM_FETCHED': 
    return {
      ...state, 
      item: action.payload,
      Reserved: action.payload.Reserved,
      loading: true
    }
  case 'ERROR_SCHEDULE_ITEM_FETCHED': 
    return {
      ...state 
    }
  case 'SET_GROUP_APPOINTMENT': 
    return {
      ...state, 
      Reserved: true,
      loading: true 
    }
  case 'ERROR_SET_GROUP_APPOINTMENT': 
    return {
      ...state,
      card: false
    }
  case 'CANCEL_GROUP_APPOINTMENT': 
    return {
      ...state, 
      Reserved: false,
      loading: true 
    }
  case 'ERROR_CANCEL_APPOINTMENT': 
    return {
      ...state, 
      Reserved: false,
      loading: true
    }
  case 'LOGOUT':
    return INITIAL_STATE 

  default: 
    return state
  }
} 
