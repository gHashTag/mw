import { combineReducers } from 'redux'
import me from './Me' 
import Masters from './Masters'
import Pay from './Pay'
import TimeTable from './Timetable'
import mail from './Mail'

export default combineReducers({
  me,
  mail,
  mastersData: Masters,
  payData: Pay,
  timeTableData: TimeTable 
})
