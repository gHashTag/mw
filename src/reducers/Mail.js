const INITIAL_STATE = {
  Reserved: false,
  loading: false 
}

export default (state = INITIAL_STATE, action) => {
  //console.log('action', action.payload)
  switch (action.type) {
  case 'SPINNER': 
    return {
      ...state, 
      loading: action.payload 
    }
  case 'SIGN_APPOINTMENT': 
    return {
      ...state, 
      Reserved: true,
      loading: false 
    }
  case 'ERROR_SIGN_APPOINTMENT': 
    return {
      ...state
    }
  case 'CLEAR_STORE': 
    return {
      ...state,
      Reserved: false 
    }
  case 'CANCEL_APPOINTMENT': 
    return {
      ...state,
      Reserved: false,
      loading: false 
    }
  case 'ERROR_CANCEL_APPOINTMENT': 
    return {
      ...state 
    }
  default: 
    return state
  }
} 
