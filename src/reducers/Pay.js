const INITIAL_STATE = {
  data: {},
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case 'PAY_REQUEST': 
    return {
      ...state, data: action.payload.response, loading: true
    }
  case 'ERROR_PAY_REQUEST':
    return INITIAL_STATE 

  default: 
    return state
  }
} 
