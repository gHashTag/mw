import React from 'react'
import { hydrate, render } from 'react-dom'
import { AsyncStorage } from 'react-native'
import createHistory from 'history/createBrowserHistory'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloProvider } from 'react-apollo'
import { createUploadLink } from 'apollo-upload-client'
import { setContext } from 'apollo-link-context'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { MemoryRouter } from 'react-router'
import Favicon from 'react-favicon'
import thunk from 'redux-thunk'
import reducers from './reducers'
import App from './App'
import fav from './favicon.ico'
import { TOKEN_KEY } from './constants'

const history = createHistory()
const rootElement = document.getElementById('root')
const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)))

const authLink = setContext(async (_, { headers }) => {
  const token = await AsyncStorage.getItem(TOKEN_KEY)
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const client = new ApolloClient({
  link: authLink.concat(createUploadLink({ uri: 'https://ecommerce-cckmievlln.now.sh/' })),
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      errorPolicy: 'none'
    },
    query: {
      errorPolicy: 'none'
    },
    mutate: {
      errorPolicy: 'none'
    }
  }
})

if (rootElement.hasChildNodes()) {
  hydrate(
    <div>
      <Favicon url={fav} />
      <ApolloProvider client={client}>
        <Provider store={store} history={history}>
          <MemoryRouter>
            <App />
          </MemoryRouter>
        </Provider>
      </ApolloProvider>
    </div>
    , rootElement)
} else {
  render(
    <div>
      <Favicon url={fav} />
      <ApolloProvider client={client}>
        <Provider store={store} history={history}>
          <MemoryRouter>
            <App />
          </MemoryRouter>
        </Provider>
      </ApolloProvider>
    </div>
    , rootElement)
}
