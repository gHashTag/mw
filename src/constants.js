import { Dimensions } from 'react-native' // eslint-disable-line

export const win = Dimensions.get('window')
export const w = win.width
export const h = win.height
export const responsive = {
  mobile5: w > 315 && w < 341,
  mobile6: w > 342 && w < 415,
  tablet: w < 990 && w > 415,
  desktop: w < 1920 && w >= 991, 
  desktopHD: w > 1920
}

export const TOKEN_KEY = '@AccountKitToken:key'
export const PHONE = '@AccountKit:phone'

export const FB = {
  facebook_app_id: '2077752889155966',
  app_secret: 'be5ddf53199b18ba0d7bee150a44b074',
  version: 'v1.3'
}

export const constants = {
  // LOCALHOST
  //SITE: 'http://localhost:3000',
  //URL: 'http://localhost:9000/api',
  //MAILGUN: 'http://localhost:9000',
  //URLFONDY: 'http://localhost:9000/fondy',
  
  // CRC.YOGA
  //SITE: 'https://www.crc.yoga',
  //URL: 'https://www.crc.yoga/api/v1', //test
  //URLFONDY: 'https://www.crc.yoga/fondy', //test
  //MERCHANT_ID: '1404059', //test
  //MERCHANT_KEY: 'nYRTUcJ6EbEcYI6uJzbMm7Zs0bbQRrUI', //test

  // CITY RETREAT
  SITE: 'https://www.cityretreat.ru',
  URL: 'https://www.cityretreat.ru/api/v1',
  MAILGUN: 'https://www.cityretreat.ru/feedback',
  //MERCHANT_ID: '1410804',
  //MERCHANT_KEY: 'kQNbxEa5PE3Ww0LCrOH4t7a70fQjzO6A',
  //URLFONDY: 'https://www.cityretreat.ru/fondy',
  PRIMARY: '#242323',
  SECONDARY: '#AB9064',
  GOLDOPACITY: 'rgba(171, 144, 100, 0.1)',
  OPACITY: 'rgba(225, 225, 225, 0.3)',
  BLACK: '#242323',
  WHITE: '#FFFFFF',
  LIGHT_GRAY: '#CAD0D6',
  FONT: 'AppleSDGothicNeo-Light',
  CirceLight: 'CirceLight',
  FONTLIGHT: 'CirceLight',
  FONTHEADER: 'Museo500',
  FONTEXTRATHIN: 'CirceExtraLight',
  FONTCOLOR: '#474747' 
}

//URL: 'https://cloud.1c.fitness/app02/199/hs/api/v1/',
