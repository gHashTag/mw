import React, { PureComponent } from 'react'
import Plx from 'react-plx'
import { Image } from '../common'

class AnimateDown extends PureComponent {
  render() {
    const { path, alt, start, duration } = this.props
    return (
      <Plx parallaxData={
        [{
          start,
          duration,
          properties: [
            {
              startValue: 100,
              endValue: 600,
              property: 'translateY'
            },
            {
              startValue: 1,
              endValue: 0,
              property: 'opacity'
            }
          ]
        }]
      } 
      style={{
        position: 'fixed',
        left: '50%',
        top: '20vh'
      }}
      >
        <Image src={path} alt={alt} />
      </Plx> 	
    )
  }
}

export default AnimateDown
