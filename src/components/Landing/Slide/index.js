import React, { PureComponent } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import Left from './Left'
import Right from './Right'
import wellness from './image/1wellness.png'
import yoga from './image/2yoga.png'
import pool from './image/3pool.png'
import spa from './image/4spa.png'
import healing from './image/5healing.png'
import children from './image/6children.png'
import food from './image/7food.png'
import design from './image/design.png'
import medicine from './image/9clinic.png'
import events from './image/10events.png'
import education from './image/11education.png'
import travels from './image/12travels.png'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w <= 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

const info1 = 'Единственный в Москве Wellness особняк Premium сегмента, расположенный в самом сердце мегаполиса. Мы предлагаем повышенный уровень комфорта и сервиса, а так же инновационные разработки в сфере wellness - услуг.'
const info2 = 'Групповые и персональные классы по йоге. Мы предлагаем более 10 различных видов йоги: хатха-йога, кундалини йога, yoga23, крия-йога, медитация, йога критического выравнивания...'
const info3 = 'Бассейн с  адаптивной системой, позволяющей регулировать температуру воды в зависимости от класса. Авторские программы по аква - фитнесу и аква - расслаблению от ведущих Российских специалистов.'
const info4 = 'Клуб оснащен самым современным оборудованием, а интерьер и дизайнерское решение заставляют почувствовать себя здесь как дома, даря ощущение безопасности и нахождения в правильном месте в правильное время.'
const info5 = 'С beauty-корнером, кабинетами для массажа, физиотерапевтических процедур и косметологии.'
const info6 = 'Мастера спорта и лучшие тренеры Москвы по самым востребованным видам групповых классов, а так же эксклюзивные специалисты, работающие на инновационном тренажере Правило.'
const info7 = 'Ваших маленьких спортсменов ждут программы, развивающие физические качества, координацию, концентрацию, логическое мышление. Классы хореографии, танцевальные уроки, групповые программы, единоборства и занятия в бассейне заинтересуют и приведут детей в восторг.'
const info8 = 'Здоровое питание в эко баре.'
const info9 = 'Консультации ведущих зарубежных специалистов в области травматологии, эндокринологии, урологии, восстановительной медицины, ортопедии, гинекологии, а так же anti-age медицины.'
const info10 = 'Клубные мероприятия.'
const info11 = 'Лекции и семинары c опытными мастерами.'
const info12 = 'Путешествия в уникальные места  планеты и лучшие отели Мира.'

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    height: mobile5 ? 4500 : mobile6 ? 4800 : mobile6plus ? 5000 : tablet ? 2500 : desktop ? 6000 : desktopHD ? 8000 : 0
  }
})

class Slide extends PureComponent {
  render() {
    const { container } = styles
    return (
      <View style={container}>
        <Left title='Wellness' info={info1} image={wellness} page='/gallery/0' />
        <Right title='Yoga' info={info2} image={yoga} page='/timetable' />
        <Left title='Aqua' info={info3} image={pool} page='/pool' />
        <Right title='Design' info={info4} image={design} page='/gallery/0' />
        <Left title='Spa' info={info5} image={spa} />
        <Right title='Teachers' info={info6} image={healing} />
        <Left title='Children' info={info7} image={children} page='/pool' />
        <Right title='Food' info={info8} image={food} page='/bar' />
        <Left title='Medicine' info={info9} image={medicine} />
        <Right title='Events' info={info10} image={events} page='/events/yoga23' />
        <Left title='Education' info={info11} image={education} />
        <Right title='Travels' info={info12} image={travels} page='/travels/top6' />
      </View>
    )
  }
}

export default Slide 
