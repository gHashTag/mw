import React, { PureComponent } from 'react'
import { View, Text, Image, Dimensions } from 'react-native'
import ReactHoverObserver from 'react-hover-observer'
import { withRouter } from 'react-router'
import AOS from 'aos'
import { ButtonUI } from '../../common'
import './styles.css'
import '../../aos.css'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w <= 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w <= 1024 && w > 415
const desktop = w < 1920 && w > 1024 
const desktopHD = w >= 1920

const styles = {
  imageStyle: {
    height: mobile5 ? 158 : mobile6 ? w - 185 : mobile6plus ? w - 205 : tablet ? 280 : desktop ? 400 : desktopHD ? 562 : 0,
    width: mobile5 ? w - 40 : mobile6 ? w - 40 : mobile6plus ? w - 40 : tablet ? 500 : desktop ? 710 : desktopHD ? 1000 : 0,
    marginBottom: tablet ? 80 : desktop ? 100 : desktopHD ? 100 : 0
  },
  h1: {
    fontFamily: 'Museo500', 
    fontSize: mobile5 ? 30 : mobile6 ? 40 : mobile6plus ? 40 : tablet ? 80 : desktop ? 90 : desktopHD ? 100 : 30,
    paddingTop: mobile5 ? 50 : mobile6 ? 20 : mobile6plus ? 20 : tablet ? 0 : desktop ? 0 : 0 
  },
  line: {
    left: mobile5 ? '100%' : mobile6 ? '100%' : mobile6plus ? '100%' : desktop ? 22 : desktopHD ? 25 : 20,
    top: desktop ? 86 : desktopHD ? 96 : tablet ? 77 : 0,
    width: mobile5 ? w - 40 : mobile6 ? w - 40 : mobile6plus ? w - 40 : tablet ? 350 : desktop ? 400 : desktopHD ? 600 : '',
    position: mobile5 ? '' : mobile6 ? '' : mobile6plus ? '' : 'absolute'
  },
  button: { 
    justifyContent: mobile5 ? 'center' : mobile6 ? 'center' : mobile6plus ? 'center' : 'flex-start',
    alignItems: mobile5 ? 'center' : mobile6 ? 'center' : mobile6plus ? 'center' : 'flex-start',
    paddingTop: 20
  },
  textContainer: {
    flexDirection: 'row',
    width: mobile5 ? w - 40 : mobile6 ? w - 40 : mobile6plus ? w - 40 : tablet ? 300 : desktop ? 370 : desktopHD ? 500 : ''
  },
  h2: {
    color: '#707070', 
    fontFamily: 'CirceExtraLight', 
    fontSize: mobile5 ? 14 : mobile6 ? 15 : mobile6plus ? 15 : tablet ? 15 : desktop ? 17 : desktopHD ? 19 : '',
    paddingTop: mobile5 ? 10 : mobile6 ? 10 : mobile6plus ? 10 : tablet ? 0 : desktop ? 0 : '',
    paddingBottom: mobile5 ? 10 : mobile6 ? 10 : mobile6plus ? 10 : tablet ? 0 : desktop ? 0 : '',
    lineHeight: mobile5 ? 15 : desktopHD ? 25 : 20,
    textAlign: 'left' 
  },
  h2big: {
    color: '#000', 
    fontFamily: 'Museo500', 
    fontSize: mobile5 ? 20 : mobile6 ? 25 : mobile6plus ? 25 : tablet ? 29 : desktop ? 40 : desktopHD ? 45 : '',
    lineHeight: 1, 
    bottom: 0 
  }
}

AOS.init()
class Index extends PureComponent {
  goToPage = () => {
    const { page } = this.props
    this.props.history.push(page)
  }

  render() {
    const { imageStyle, button, line, h1, textContainer, h2, h2big } = styles
    const { title, page, info, image } = this.props

    return (
      <View>
        { desktopHD === true &&
          <View style={{ flexDirection: 'row' }}>
            <Image 
              data-aos="zoom-in"
              data-aos-easing="ease-out-cubic"
              data-aos-duration="1000"
              data-aos-anchor-placement="bottom-bottom"
              source={image} 
              style={imageStyle} 
            />
            <View style={{ paddingLeft: 20, marginTop: 30 }}>
              <View 
                data-aos='fade-up'
                data-aos-duration="2000"
                data-aos-anchor-placement="bottom-bottom"
                style={line} 
                className='stripe rightline' 
              />
              <Text 
                data-aos='fade-down'
                data-aos-duration='2000' 
                data-aos-anchor-placement="bottom-bottom"
                style={h1}
              >{title}</Text>
              <View 
                data-aos='fade'
                data-aos-duration='2000' 
                data-aos-anchor-placement="bottom-bottom"
                style={textContainer}
              >
                <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
              </View>
              { page !== undefined &&
                <View 
                  data-aos="flip-up"
                  data-aos-duration='2000' 
                  data-aos-anchor-placement="bottom-bottom"
                  style={button} 
                >
                  <ReactHoverObserver>
                    <ButtonUI onPress={this.goToPage} title='Подробнее' />
                  </ReactHoverObserver>
                </View>
              }
            </View>
          </View>
        }

        { desktop === true &&
          <View style={{ flexDirection: 'row' }}>
            <Image 
              data-aos="zoom-in"
              data-aos-easing="ease-out-cubic"
              data-aos-duration="1000"
              data-aos-anchor-placement="bottom-bottom"
              source={image} 
              style={imageStyle} 
            />
            <View style={{ paddingLeft: 20, marginTop: 30 }}>
              <View 
                data-aos='fade-up'
                data-aos-duration="2000"
                data-aos-anchor-placement="bottom-bottom"
                style={line} 
                className='stripe rightline' 
              />
              <Text 
                data-aos='fade-down'
                data-aos-duration='2000' 
                data-aos-anchor-placement="bottom-bottom"
                style={h1}
              >{title}</Text>
              <View 
                data-aos='fade'
                data-aos-duration='3000' 
                data-aos-anchor-placement="bottom-bottom"
                style={textContainer}
              >
                <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
              </View>
              { page !== undefined &&
                <View 
                  data-aos="flip-up"
                  data-aos-duration='2000' 
                  data-aos-anchor-placement="bottom-bottom"
                  style={button} 
                >
                  <ReactHoverObserver>
                    <ButtonUI onPress={this.goToPage} title='Подробнее' />
                  </ReactHoverObserver>
                </View>
              }
            </View>
          </View>
        }

        { tablet === true &&
          <View style={{ flexDirection: 'row' }}>
            <Image 
              data-aos="zoom-in"
              data-aos-easing="ease-out-cubic"
              data-aos-duration="1000"
              data-aos-anchor-placement="bottom-bottom"
              source={image} 
              style={[imageStyle, { marginLeft: 50 }]} 
            />
            <View style={{ paddingLeft: 20, marginTop: 10 }}>
              <View 
                data-aos='fade-up'
                data-aos-anchor-placement="bottom-bottom"
                style={line} 
                className='stripe rightline' 
              />
              <Text 
                data-aos='fade-down'
                data-aos-duration='1000' 
                data-aos-anchor-placement="bottom-bottom"
                style={h1}
              >{title}</Text>
              <View 
                data-aos='fade'
                data-aos-duration='2000' 
                data-aos-anchor-placement="bottom-bottom"
                style={textContainer}
              >
                <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
              </View>
              { page !== undefined &&
                <View 
                  data-aos="flip-up"
                  data-aos-duration='1000' 
                  data-aos-anchor-placement="bottom-bottom"
                  style={button} 
                >
                  <ReactHoverObserver>
                    <ButtonUI onPress={this.goToPage} title='Подробнее' />
                  </ReactHoverObserver>
                </View>
              }
            </View>
          </View>
        }

        { mobile6plus === true &&
          <View>
            <Text style={h1}>{title}</Text>
            <View style={line} className='stripe rightline' />
            <View style={textContainer} >
              <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
            </View>
            <Image source={image} style={imageStyle} />
            { page !== undefined &&
              <View style={button}>
                <ReactHoverObserver>
                  <ButtonUI onPress={this.goToPage} title='Подробнее' />
                </ReactHoverObserver>
              </View>
            }
          </View>
        }

        { mobile6 === true &&
          <View>
            <Text style={h1}>{title}</Text>
            <View style={line} className='stripe rightline' />
            <View style={textContainer} >
              <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
            </View>
            <Image source={image} style={imageStyle} />
            { page !== undefined &&
              <View style={button}>
                <ReactHoverObserver>
                  <ButtonUI onPress={this.goToPage} title='Подробнее' />
                </ReactHoverObserver>
              </View>
            }
          </View>
        }

        { mobile5 === true &&
          <View>
            <Text style={h1}>{title}</Text>
            <View style={line} className='stripe rightline' />
            <View style={textContainer}>
              <Text style={h2}><Text style={h2big}>{info.charAt(0)}</Text>{info.substring(1)}</Text>
            </View>
            <Image source={image} style={imageStyle} />
            { page !== undefined &&
              <View style={button}>
                <ReactHoverObserver>
                  <ButtonUI onPress={this.goToPage} title='Подробнее' />
                </ReactHoverObserver>
              </View>
            }
          </View>
        }
      </View>
    )
  }
}

const Right = withRouter(Index)

export default Right 
