import React, { Component } from 'react'
import { TouchableHighlight, View, Image, Dimensions } from 'react-native'
import Plx from 'react-plx'
import { withRouter } from 'react-router'
import { constants } from '../../constants'
import frame from '../../actions/masters/frame.png'
import felix from '../../actions/masters/m12.png'
import baykov from '../../actions/masters/m19.png'
import rudnitskaya from '../../actions/masters/m24.png'
import marshak from '../../actions/masters/m22.png'
import mirzabekyan from '../../actions/masters/m27.png'
import levchenko from '../../actions/masters/m4.png'
import mitus from '../../actions/masters/m16.png'
import fateeva from '../../actions/masters/m20.png'
import reutov from '../../actions/masters/m28.png'
import dubnel from '../../actions/masters/m0.png'
import husnulin from '../../actions/masters/m26.png'
import visotskiy from '../../actions/masters/m2m.png'
import ahmetgareev from '../../actions/masters/m25.png'
import fedorov from '../../actions/masters/m1.png'
import chulibaev from '../../actions/masters/m23.png'
import mesherykova from '../../actions/masters/m9.png'
import semicheva from '../../actions/masters/m14.png'
import mizilin from '../../actions/masters/m8.png'
import hapkova from '../../actions/masters/m11copy.png'
import MastersCommon from '../common/MastersCommon'

const win = Dimensions.get('window')
const w = win.width
//const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const styles = {
  felixStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: -125,
    opacity: 0,
    bottom: 0
  },
  baykovStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: -60,
    opacity: 0,
    bottom: 0
  },
  rudnitskayaStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: -25,
    opacity: 0,
    bottom: 0
  },
  marshakStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 0,
    opacity: 0,
    bottom: 0
  },
  mirzabekyanStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 50,
    opacity: 0,
    bottom: 0
  },
  levchenkoStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 80,
    opacity: 0,
    bottom: 0
  },
  mitusStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 170,
    opacity: 0,
    bottom: 0
  },
  fateevaStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 220,
    opacity: 0,
    bottom: 0
  },
  reutovStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 270,
    opacity: 0,
    bottom: 0
  },
  dubnelStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 390,
    opacity: 0,
    bottom: 0
  },
  husnulinStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 340,
    opacity: 0,
    bottom: 0
  },
  visotskiyStyle: {
    left: '50%',
    position: 'absolute',
    marginLeft: 430,
    opacity: 0,
    bottom: 0
  },
  ahmetgareevStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 100,
    opacity: 0,
    bottom: 0
  },
  fedorovStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 170,
    opacity: 0,
    bottom: 0
  },
  chulibaevStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 230,
    opacity: 0,
    bottom: 0
  },
  mesherykovaStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 290,
    opacity: 0,
    bottom: 0
  },
  semichevaStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 350,
    opacity: 0,
    bottom: 0
  },
  mizilinStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 430,
    opacity: 0,
    bottom: 0
  },
  hapkovaStyle: {
    right: '50%',
    position: 'absolute',
    marginRight: 350,
    opacity: 0,
    bottom: 0
  },
  ourStyle: {
    top: mobile5 ? 40 : mobile6 ? 50 : tablet ? 60 : desktop ? 60 : '', //eslint-disable-line
    width: mobile5 ? 240 : mobile6 ? 260 : tablet ? 260 : desktop ? 280 : '', //eslint-disable-line
    height: mobile5 ? 200 : mobile6 ? 217 : tablet ? 217 : desktop ? 234 : '', //eslint-disable-line
  },
  frameStyle: {
    opacity: 0
  },
  imageStyle: {
    height: 250,
    width: 250
  }
}

const h = 1500

const parallaxDataFrame = [
  { start: h, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' },
    { startValue: 3, endValue: 0, property: 'blur' }
  ]
  }
]

const parallaxDataFelix = [
  { start: h + 200, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' }
  ]
  }
]

const parallaxDataBaykov = [
  { start: h + 250, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' },
    { startValue: 30, endValue: 0, property: 'translateX' }
  ]
  }
]

const parallaxDataRudnitskaya = [
  { start: h + 250, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' },
    { startValue: -30, endValue: 0, property: 'translateX' }
  ]
  }
]

const parallaxDataCommonRight = [
  { start: h + 270, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' },
    { startValue: 30, endValue: 0, property: 'translateX' },
    { startValue: 3, endValue: 0, property: 'blur' }
  ]
  }
]

const parallaxDataCommonLeft = [
  { start: h + 270, duration: 100, properties: [
    { startValue: 0, endValue: 1, property: 'opacity' },
    { startValue: -30, endValue: 0, property: 'translateX' },
    { startValue: 3, endValue: 0, property: 'blur' }
  ]
  }
]

const url = '/master/feliks-pak'

class Index extends Component {
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    //console.log('h', window.scrollY)
  }

  handlePress() {
    this.props.history.push(url)
  }

  render() {
    const { semichevaStyle, hapkovaStyle, mizilinStyle, felixStyle, mesherykovaStyle, chulibaevStyle, fedorovStyle, ahmetgareevStyle, visotskiyStyle, husnulinStyle, dubnelStyle, reutovStyle, fateevaStyle, baykovStyle, rudnitskayaStyle, mitusStyle, marshakStyle, mirzabekyanStyle, levchenkoStyle, frameStyle, ourStyle, imageStyle } = styles
    return (
      <View>
        { desktop === true &&
          <View style={{ backgroundColor: '#fff', height: 600 }}>
            <Plx parallaxData={parallaxDataCommonRight} style={reutovStyle}>
              <Image source={reutov} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={dubnelStyle}>
              <Image source={dubnel} style={{ height: 230, width: 230 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={visotskiyStyle}>
              <Image source={visotskiy} style={{ height: 250, width: 210 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={husnulinStyle}>
              <Image source={husnulin} style={{ height: 220, width: 220 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={fateevaStyle}>
              <Image source={fateeva} style={{ height: 230, width: 230 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={mitusStyle}>
              <Image source={mitus} style={{ height: 230, width: 230 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={mirzabekyanStyle}>
              <Image source={mirzabekyan} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={ahmetgareevStyle}>
              <Image source={ahmetgareev} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={chulibaevStyle}>
              <Image source={chulibaev} style={{ height: 260, width: 260 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={fedorovStyle}>
              <Image source={fedorov} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={mesherykovaStyle}>
              <Image source={mesherykova} style={{ height: 250, width: 250 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={marshakStyle}>
              <Image source={marshak} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonRight} style={levchenkoStyle}>
              <Image source={levchenko} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={semichevaStyle}>
              <Image source={semicheva} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={hapkovaStyle}>
              <Image source={hapkova} style={{ height: 300, width: 290 }} />
            </Plx>
            <Plx parallaxData={parallaxDataCommonLeft} style={mizilinStyle}>
              <Image source={mizilin} style={{ height: 250, width: 210 }} />
            </Plx>
            <Plx parallaxData={parallaxDataRudnitskaya} style={rudnitskayaStyle}>
              <Image source={rudnitskaya} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataBaykov} style={baykovStyle}>
              <Image source={baykov} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataFelix} style={felixStyle}>
              <Image source={felix} style={imageStyle} />
            </Plx>
            <Plx parallaxData={parallaxDataFrame} style={frameStyle}>
              <TouchableHighlight
                activeOpacity={0.2}
                underlayColor={constants.GOLDOPACITY} 
                onPress={() => this.handlePress()}
              >
                <Image source={frame} style={ourStyle} />
              </TouchableHighlight>
            </Plx>
          </View>
        }
        { tablet === true &&
          <MastersCommon />
        }
        { mobile6 === true &&
          <MastersCommon />
        }
        { mobile5 === true &&
          <MastersCommon />
        }
      </View>
    )
  }
}

const Masters = withRouter(Index)
export default Masters 
