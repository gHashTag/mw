import React from 'react'
import { Dimensions } from 'react-native'
import { Helmet } from 'react-helmet'
import Table from './Table'
import { Header } from '../common'
import Door from '../common/Door'
import MouseScroller from '../common/MouseScroller'
import logo from '../img/logo.jpg'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default () => (
  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Расписание·Mansion Wellness</title>
      <link rel="canonical" href="http://cityretreat.ru/price" />
      <meta name="keywords" content="йога, сити ретрит, spa, wellness, спа, велнес" />
      <meta property="og:image" content={logo} />
      <meta name="description" content="Особняк в центре Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы. Уникальный формат Центра – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей." />
    </Helmet>

    <Header title="Расписание">
      { desktopHD === true &&
        <Door height={2000}>
          <Table />
        </Door>
      }
      { desktop === true &&
        <Door height={2000}>
          <Table />
        </Door>
      }
      { tablet === true &&
        <Door height={2000}>
          <Table />
        </Door>
      }
      { mobile6plus === true &&
        <Table />
      }
      { mobile6 === true &&
        <Table />
      }
      { mobile5 === true &&
        <Table />
      }
    </Header>
    <MouseScroller />
  </div>
) 
