import React, { Component } from 'react'
import { Dimensions, View, Text } from 'react-native'
import ReactHoverObserver from 'react-hover-observer'
import { withRouter } from 'react-router'
import 'moment/locale/ru'
import { connect } from 'react-redux'
import { getSchedule, getScheduleItem } from '../../actions'
import Accordion from '../common/Collapsible/Accordion'
import { ButtonUI, Spinner } from '../common'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Index extends Component {
  state = {
    collapsed: true,
    activeSection: false,
    data: [],
    status: false 
  }

  async componentDidMount() {
    const { StartDate, EndDate } = this.props
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'getSchedule',
        Parameters: { 
          StartDate: StartDate.format('YYYY-MM-DD HH:mm'), 
          EndDate: EndDate.format('YYYY-MM-DD HH:mm') 
        }
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const data = await res.json()
      this.setState({ data })
    } catch (e) {
      throw e
    }

    this.props.getSchedule({ StartDate, EndDate })    
  }

  _toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  _setSection = section => {
    this.setState({ activeSection: section })
  }

  _renderButton = (section) => {
    return (
      <View style={{ paddingTop: 10, paddingBottom: 0, alignSelf: 'center' }}>
        <ReactHoverObserver>
          <ButtonUI style={{ alignItems: 'center' }} title='Запись' onPress={() => this.props.history.push('./reserve', section)} />
        </ReactHoverObserver>
      </View>
    )
  }

  _renderHeader = (section, i, isActive) => {
    const { h2, digits } = styles
    const { StartDate, Service, Employee } = section
    return (
      <View>
        { desktop === true &&
          <View style={[{ marginTop: 10, width: 800 }, styles.content, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={[digits, { fontSize: 24, textAlign: 'left' }]}> {StartDate.split(' ')[1]}</Text>
                <Text style={[h2, { fontSize: 22, justifyContent: 'flex-start', paddingLeft: 50 }]}>{Service.Title}</Text>
              </View>
              <View>
                <Text style={[h2, { fontSize: 22 }]}>{Employee.FullName} </Text>
              </View>
            </View>
          </View>
        }
        { tablet === true &&
          <View style={[{ flex: 1, marginTop: 10, width: 600 }, styles.content, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
            <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                <Text style={[digits, { fontSize: 18, textAlign: 'left' }]}> {StartDate.split(' ')[1]}</Text>
                <Text style={[h2, { fontSize: 18, justifyContent: 'flex-start', paddingLeft: 30 }]}>{Service.Title}</Text>
              </View>
              <View>
                <Text style={[h2, {}]}>{Employee.FullName} </Text>
              </View>
            </View>
          </View>
        }
        { mobile6 === true &&
          <View style={[{ flex: 1, marginTop: 10, width: 360 }, styles.content, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
            <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                <View style={{ width: 50 }}>
                  <Text style={[digits, { fontSize: 13, textAlign: 'left' }]}> {StartDate.split(' ')[1]}</Text>
                </View>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[h2, { fontSize: 13, justifyContent: 'flex-start', paddingLeft: 5 }]}>{Service.Title}</Text>
              </View>
              <View>
                <Text style={[h2, { fontSize: 12 }]}>{Employee.FullName} </Text>
              </View>
            </View>
          </View>
        }
        { mobile5 === true &&
          <View style={[{ flex: 1, marginTop: 5, width: 290 }, styles.content, isActive ? styles.active : styles.inactive]} transition="backgroundColor">
            <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                <View style={{ width: 40 }}>
                  <Text style={[digits, { fontSize: 11, textAlign: 'left' }]}> {StartDate.split(' ')[1]}</Text>
                </View>
                <Text numberOfLines={1} ellipsizeMode='tail' style={[h2, { fontSize: 11, justifyContent: 'flex-start', paddingLeft: 5 }]}>{Service.Title}</Text>
              </View>
              <View>
                <Text style={[h2, { fontSize: 12 }]}>{Employee.FullName} </Text>
              </View>
            </View>
          </View>
        }
      </View>
    )
  }

  _renderContent = (section, i, isActive) => {
    const description = section.Service.Description
    const regex = /(<([^>]+)>)/ig  
    const content = description === null ? '' : description.replace(regex, '')
    const { h1, h2 } = styles
    return (
      <View>
        { desktop === true &&
          <View style={[{ paddingTop: 30, paddingLeft: 20, paddingRight: 20, paddingBottom: 20, width: 800 }, styles.content, isActive ? styles.active : styles.inactive]}>
            <Text style={[h2, { color: '#707070', fontSize: 17, lineHeight: 20 }]}>
              <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1 }}>
                {content.charAt(0)}
              </Text>
              {content.substring(1)}
            </Text>
            {this._renderButton(section)}
          </View>
        }
        { tablet === true &&
          <View style={[{ flex: 1, paddingTop: 40, paddingLeft: 20, paddingRight: 20, paddingBottom: 20, width: 600 }, styles.content, isActive ? styles.active : styles.inactive]}>
            <Text style={[h2, { color: '#707070', fontSize: 17, lineHeight: 18 }]}>
              <Text style={[h1, { color: '#000', fontSize: 40, lineHeight: 1, bottom: 0 }]}>
                {content.charAt(0)}
              </Text>
              {content.substring(1)}
            </Text>
            {this._renderButton(section)}
          </View>
        }
        { mobile6 === true &&
          <View style={[{ flex: 1, paddingTop: 20, paddingLeft: 20, paddingRight: 20, paddingBottom: 20, width: 350 }, styles.content, isActive ? styles.active : styles.inactive]}>
            <Text style={[h2, { color: '#707070', fontSize: 12, lineHeight: 16 }]}>
              <Text style={[h1, { color: '#000', fontSize: 20, lineHeight: 1, bottom: 0 }]}>
                {content.charAt(0)}
              </Text>
              {content.substring(1)}
            </Text>
            {this._renderButton(section)}
          </View>
        }
        { mobile5 === true &&
          <View style={[{ flex: 1, paddingTop: 10, paddingLeft: 10, paddingRight: 10, paddingBottom: 15, width: 290 }, styles.content, isActive ? styles.active : styles.inactive]}>
            <Text style={[h2, { color: '#707070', fontSize: 11, lineHeight: 11 }]}>
              <Text style={[h1, { color: '#000', fontSize: 17, lineHeight: 1, bottom: 0 }]}>
                {content.charAt(0)}
              </Text>
              {content.substring(1)}
            </Text>
            {this._renderButton(section)}
          </View>
        }
      </View>
    )
  }

  render() {
    const { Parameters } = this.state.data
    const timetable = Parameters || [] 
    const { loading } = this.props.data
    const ClientID = this.props.ClientID
    const { h1 } = styles
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center' }} >
        { desktop === true &&
          <div style={{ marginTop: 50 }}>
            <Text style={[h1, { fontSize: 40, textAlign: 'right' }]} >
              { this.props.StartDate.format('dddd, DD, MMMM') }
            </Text>
            { !loading && <View style={{ paddingTop: 20 }}><Spinner /></View> } 
          </div>
        }
        { tablet === true &&
          <div style={{ marginTop: 30 }}>
            <Text style={[h1, { fontSize: 30, textAlign: 'right' }]} >
              {this.props.StartDate.format('dddd, DD, MMMM')}
            </Text>
            { !loading && <View style={{ paddingTop: 20 }}><Spinner /></View> } 
          </div>
        }
        { mobile6 === true &&
          <div style={{ marginTop: 10 }}>
            <Text style={[h1, { fontSize: 24, textAlign: 'right' }]} >
              {this.props.StartDate.format('dddd, DD, MMMM')}
            </Text>
            { !loading && <View style={{ paddingTop: 20 }}><Spinner /></View> } 
          </div>
        }
        { mobile5 === true &&
          <div style={{ marginTop: 10 }}>
            <Text style={{ fontFamily: 'Museo500', fontSize: 18, textAlign: 'right' }} >
              {this.props.StartDate.format('dddd, DD, MMMM')}
            </Text>
            { !loading && <View style={{ paddingTop: 20 }}><Spinner /></View> } 
          </div>
        }
        <Accordion
          activeSection={this.state.activeSection}
          sections={timetable}
          underlayColor='#D1AE6C'
          ClientID={ClientID}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
          onChange={this._setSection}
        />
      </View>
    )
  }
}

const styles = {
  h1: {
    fontFamily: 'Museo500' 
  },
  digits: {
    fontFamily: 'Arnamu'
  },
  h2: {
    fontFamily: 'CirceExtraLight' 
  }
}

const mapStateToProps = state => {
  return { 
    data: state.timeTableData,
    info: state.me.info,
    clientData: state.me.clientData
  }
}

const Day = withRouter(Index)

export default connect(mapStateToProps, { getSchedule, getScheduleItem })(Day)

