import React, { Component } from 'react'
import { AsyncStorage, Dimensions, View, Text } from 'react-native' 
import moment from 'moment'
import { connect } from 'react-redux'
import { getMe, getClientByPhone } from '../../actions'
import { PHONE } from '../../constants'
import Day from './Day'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

const sale = 'Запись на занятия ОБЯЗАТЕЛЬНА!'

const menuY = (x) => {
  try {
    if (w <= 321) {
      return x + 80 //'iphone5'
    } else if (w <= 375 && w > 322) {
      return x + 85 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x + 90 //'iphone6+'
    } else if (w > 414) {
      return x + 300// 'web'
    }
  } catch (e) {
    throw e
  }
}

class Table extends Component {
  state = { error: '' }

  async componentDidMount() {
    this._getInfo()
  }

  _getInfo = async () => { 
    try {
      const Phone = await AsyncStorage.getItem(PHONE)
      this.props.getClientByPhone(Phone)
    } catch (e) {
      const error = this.props.data.error.message
      this.setState({ error })
    }
  }

  render() {
    const { ClientID } = this.props.clientData
    const { headerView, description } = styles
    return (
      <View style={{ marginTop: mobile5 ? 0 : mobile6plus ? 0 : mobile6 ? 0 : h }}>
        <View style={{ marginTop: menuY(1), marginBottom: 250 }}>
          <View style={[{ marginTop: 0 }, headerView]}>
            <Text style={description}>{sale}</Text>
          </View>
          <Day 
            StartDate={moment()} 
            EndDate={moment().add(0, 'days').endOf('day')}
          />
          <Day 
            ClientID={ClientID} 
            StartDate={moment().add(1, 'days').startOf('day')}
            EndDate={moment().add(1, 'days').endOf('day')}
          />
          <Day 
            StartDate={moment().add(2, 'days').startOf('day')}
            EndDate={moment().add(2, 'days').endOf('day')}
          />
          <Day 
            StartDate={moment().add(3, 'days').startOf('day')}
            EndDate={moment().add(3, 'days').endOf('day')}
          />
          <Day 
            StartDate={moment().add(4, 'days').startOf('day')}
            EndDate={moment().add(4, 'days').endOf('day')}
          />
          <Day 
            StartDate={moment().add(5, 'days').startOf('day')}
            EndDate={moment().add(5, 'days').endOf('day')}
          />
          <Day 
            StartDate={moment().add(6, 'days').startOf('day')}
            EndDate={moment().add(6, 'days').endOf('day')}
          />
        </View>
      </View>
    )
  }
}

const styles = {
  headerView: {
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center'
  },
  description: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'CirceLight', 
    marginLeft: 20,
    marginRight: 20,
    fontSize: mobile5 ? 12 : mobile6 ? 15 : tablet ? 12 : desktop ? 16 : desktopHD ? 19 : 15
  }
}

const mapStateToProps = state => {
  return { 
    info: state.me.info,
    clientData: state.me.clientData
  }
}

export default connect(mapStateToProps, { getMe, getClientByPhone })(Table)

