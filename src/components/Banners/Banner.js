import React, { Component } from 'react'
import { Platform, TouchableHighlight, View, Image, Dimensions } from 'react-native'
import 'rc-banner-anim/assets/index.css'
import BannerAnim from 'rc-banner-anim'
import TweenOne from 'rc-tween-one'
import { withRouter } from 'react-router'
import './events.css'
import ArrowNext from '../ArrowNext.png'
import ArrowBack from '../ArrowBack.png'
import A from './image/saying.png'
import B from './image/sistem432.png'
import C from './image/ycv.png'
import D from './image/yoga23.png'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992
const { Element, Arrow, Thumb } = BannerAnim
const BgElement = Element.BgElement

const styles = {
  framestyle: {
    position: 'fixed', 
    zIndex: 1,
    left: (win.width * 0.5) - 150,
    top: mobile5 ? 40 : mobile6 ? 50 : tablet ? 60 : desktop ? 60 : '',
    width: mobile5 ? 300 : 300, 
    height: mobile5 ? 228 : 228
  },
  arrowback: {
    margin: 20, 
    height: mobile5 ? 60 : mobile6 ? 60 : 100,
    width: mobile5 ? 60 : mobile6 ? 60 : 100
  },
  arrownext: {
    margin: 20, 
    height: mobile5 ? 60 : mobile6 ? 60 : 100,
    width: mobile5 ? 60 : mobile6 ? 60 : 100
  }
}

const url0 = '/events/saying'
const url1 = '/timetable'
const url2 = '/timetable'
const url3 = '/events/yoga23'

class Index extends Component {
  constructor(props) {
    super(props)
    this.imgArray = [
      A,
      B,
      C,
      D
    ]
    this.state = {
      intShow: 0,
      prevEnter: false,
      nextEnter: false,
      thumbEnter: false
    }
  }

  onChange = (type, int) => {
    if (type === 'before') {
      this.setState({
        intShow: int
      })
    }
  }

  onMouseEnter = () => {
    this.setState({
      thumbEnter: true
    })
  }

  onMouseLeave = () => {
    this.setState({
      thumbEnter: false
    })
  }

  getNextPrevNumber = () => {
    let nextInt = this.state.intShow + 1
    let prevInt = this.state.intShow - 1
    if (nextInt >= this.imgArray.length) {
      nextInt = 0
    }
    if (prevInt < 0) {
      prevInt = this.imgArray.length - 1
    }

    return [prevInt, nextInt]
  }

  prevEnter = () => {
    this.setState({
      prevEnter: true
    })
  }

  prevLeave = () => {
    this.setState({
      prevEnter: false
    })
  }

  nextEnter = () => {
    this.setState({
      nextEnter: true
    })
  }

  nextLeave = () => {
    this.setState({
      nextEnter: false
    })
  }
  
  handlePress(url) {
    this.props.history.push(url)
  }

  render() {
    const { container, framestyle, arrowback, arrownext } = styles
    const thumbChildren = this.imgArray.map(
      (img, i) =>
      <span key={i}> 
        <i style={{ backgroundImage: `url(${img})` }} />
      </span>
    )

    return (
      <BannerAnim 
        onChange={this.onChange} 
        onMouseEnter={this.onMouseEnter} 
        onMouseLeave={this.onMouseLeave} 
        prefixCls="custom-arrow-thumb"
      >
        <Element key="aa1" prefixCls="banner-user-elem" >
          <BgElement
            key="bg"
            className="bg"
            style={{
              backgroundImage: `url(${this.imgArray[0]})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center'
            }}
          />
          <TweenOne className="banner-user-title" animation={{ y: 30, opacity: 0, type: 'from' }}>
            <View style={container}>
              <TouchableHighlight
                activeOpacity={0.2}
                onPress={() => this.handlePress(url0)}
              >
                <Image source={require('./image/frame-saying.png')} style={framestyle} />
              </TouchableHighlight>
            </View>
          </TweenOne>
        </Element>

        <Element key="aaa" prefixCls="banner-user-elem" >
          <BgElement
            key="bg"
            className="bg"
            style={{
              backgroundImage: `url(${this.imgArray[1]})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center'
            }}
          />
          <TweenOne className="banner-user-title" animation={{ y: 30, opacity: 0, type: 'from' }}>
            <View style={container}>
              <TouchableHighlight
                activeOpacity={0.2}
                onPress={() => this.handlePress(url1)}
              >
                <Image source={require('./image/frame-sistem432.png')} style={framestyle} />
              </TouchableHighlight>
            </View>
          </TweenOne>
        </Element>

        <Element key="bbb" prefixCls="banner-user-elem" >
          <BgElement
            key="bg"
            className="bg"
            style={{
              backgroundImage: `url(${this.imgArray[2]})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center'
            }}
          />
          <TweenOne className="banner-user-title" animation={{ y: 30, opacity: 0, type: 'from' }}>
            <TouchableHighlight
              onPress={() => this.handlePress(url2)}
              activeOpacity={0.2}
            >
              <Image source={require('./image/frame-ycv.png')} style={framestyle} />
            </TouchableHighlight>
          </TweenOne>
        </Element>

        <Element key="ccc" prefixCls="banner-user-elem" >
          <BgElement
            key="bg"
            className="bg"
            style={{
              backgroundImage: `url(${this.imgArray[3]})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center'
            }}
          />
          <TweenOne className="banner-user-title" animation={{ y: 30, opacity: 0, type: 'from' }}>
            <TouchableHighlight
              onPress={() => this.handlePress(url3)}
              activeOpacity={0.2}
            >
              <Image source={require('./image/frame-yoga23.png')} style={framestyle} />
            </TouchableHighlight>
          </TweenOne>
        </Element>

        <Arrow arrowType="prev" key="prev" prefixCls="user-arrow" component={TweenOne}
          onMouseEnter={this.prevEnter}
          onMouseLeave={this.prevLeave}
          animation={{ left: this.state.prevEnter || (Platform.OS === 'web') ? 0 : -120 }}
        >
          <Image style={arrowback} source={ArrowBack} />
        </Arrow>
        <Arrow arrowType="next" key="next" prefixCls="user-arrow" component={TweenOne}
          onMouseEnter={this.nextEnter}
          onMouseLeave={this.nextLeave}
          animation={{ right: this.state.nextEnter || (Platform.OS === 'web') ? 0 : -120 }}
        >
          <Image style={arrownext} source={ArrowNext} />
        </Arrow>
        <Thumb prefixCls="user-thumb" key="thumb" component={TweenOne}
          animation={{ bottom: this.state.thumbEnter ? 0 : -70 }}
        >
          {thumbChildren}
        </Thumb>
      </BannerAnim>
    )
  }
}

const Banner = withRouter(Index)
export default Banner 
