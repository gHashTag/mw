import React from 'react'
import { Dimensions, View } from 'react-native'
import Masters from './Masters'
import Door from '../common/Door'
import MouseScroller from '../common/MouseScroller'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default () => (
  <div>
    { desktopHD === true &&
      <View>
        <Door height={2200}>
          <Masters /> 
        </Door>
        <MouseScroller />
      </View>
    }
    { desktop === true &&
      <View>
        <Door height={2200}>
          <Masters /> 
        </Door>
        <MouseScroller />
      </View>
    }
    { tablet === true &&
      <View>
        <Door height={2200}>
          <Masters /> 
        </Door>
        <MouseScroller />
      </View>
    }
    { mobile6plus === true &&
      <Masters /> 
    }
    { mobile6 === true &&
      <Masters /> 
    }
    { mobile5 === true &&
      <Masters /> 
    }
  </div>
)
