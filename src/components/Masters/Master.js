import React, { PureComponent } from 'react' 
import { TouchableOpacity, Dimensions, Image, View, Text } from 'react-native' 
import { find } from 'lodash'
import { Helmet } from 'react-helmet'
import { Header } from '../common'
import { data } from './data'
import './styles.css'
import ArrowNext from '../ArrowNext.png'
import ArrowBack from '../ArrowBack.png'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w <= 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default class Master extends PureComponent {
  onPressNext = match => {
    return () => {
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (data.length !== now.id + 1) {
        const next = find(data, { id: now.id + 1 })
        history.push(`/master/${next.name}`)
      } else {
        history.push(`/master/${data[0].name}`)
      }
    }
  }

  onPressBack = match => {
    return () => {
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (now.id !== 0) {
        const back = find(data, { id: now.id - 1 })
        history.push(`/master/${back.name}`)
      } else {
        const lengthdata = find(data, { id: data.length - 1 })
        history.push(`/master/${lengthdata.name}`)
      }
    }
  }

  render() {
    const { match } = this.props
    const store = find(data, { name: match.params.name })
    const { img, title, subTitle, info } = store
    const url = constants.SITE + match.url
    const keywords = `${title}, ${subTitle}`
    return (
      <View>
        <Helmet>
          <meta charSet='utf-8' />
          <title>{title}·City Retreat</title>
          <link rel='canonical' href={url} />
          <meta name='keywords' content={keywords} />
          <meta property='og:image' content={img} />
          <meta name='description' content={info} />
        </Helmet>
        { desktopHD === true &&
          <View>
            <Header title='Мастера'>

              <Image source={img} style={{ position: 'fixed', left: '13%', bottom: 0, width: 700, height: 700 }} />

              <View style={{ position: 'fixed', top: '30.4%', right: '15%', width: '40%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '25%', right: '15%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }} accessibilityRole='heading'>
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '31%', right: '15%' }} >
                <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 28, textAlign: 'right' }} accessibilityRole='heading' aria-level='2'>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '40%', top: '38%', right: '15%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 23, lineHeight: 25 }} accessibilityRole='heading' aria-level='3'>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 100, width: 100 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 100, width: 100 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { desktop === true &&
          <View>
            <Header title='Мастера'>
              <Image source={img} style={{ position: 'fixed', left: '12%', bottom: 0, width: 500, height: 500 }} />

              <View style={{ position: 'fixed', top: '30.4%', right: '15%', width: '40%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '25%', right: '15%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }} accessibilityRole='heading'>
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '31%', right: '15%' }} >
                <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 22, textAlign: 'right' }} accessibilityRole='heading' aria-level='2'>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '40%', top: '38%', right: '15%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }} accessibilityRole='heading' aria-level='3'>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 100, width: 100 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 100, width: 100 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { tablet === true &&
          <View>
            <Header title='Мастера'>
              <Image source={img} style={{ position: 'fixed', left: '0%', bottom: 0, width: 500, height: 500 }} />

              <View style={{ position: 'fixed', top: '30.7vh', right: '15%', width: '40%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '25vh', right: '15%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '32%', right: '15%' }} >
                <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 22, textAlign: 'right' }}>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '40%', top: '38%', right: '15%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '38%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 100, width: 100 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '38%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 100, width: 100 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { mobile6plus === true &&
          <View>
            <Header title='Мастера'>
              <Image source={img} style={{ position: 'fixed', left: '20%', bottom: 0, width: 260, height: 260 }} />

              <View style={{ position: 'fixed', top: '16.5vh', right: '5%', width: '70%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '13vh', right: '5%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 24, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '17.1vh', right: '5%', width: win.width - 100 }} >
                <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ flex: 1, fontFamily: 'CirceExtraLight', fontSize: 18, textAlign: 'right' }}>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '90%', top: '23vh', left: '5%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 13, lineHeight: 17 }}>
                    <Text style={{ fontFamily: 'Museo500', fontSize: 35, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substr(1, 1000)}</Text>
                </View>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image style={{ height: 60, width: 60 }} source={ArrowBack} />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image style={{ height: 60, width: 60 }} source={ArrowNext} />
                </TouchableOpacity>
              </View>
            </Header>
          </View>
        }

        { mobile6 === true &&
          <View>
            <Header title='Мастера'>
              <Image source={img} style={{ position: 'fixed', left: '19%', bottom: 0, width: 240, height: 240 }} />

              <View style={{ position: 'fixed', top: '16.5vh', right: '5%', width: '70%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '13vh', right: '5%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 24, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '17.1vh', right: '5%', width: win.width - 100 }} >
                <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ flex: 1, fontFamily: 'CirceExtraLight', fontSize: 18, textAlign: 'right' }}>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '90%', top: '23vh', left: '5%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 13, lineHeight: 17 }}>
                    <Text style={{ fontFamily: 'Museo500', fontSize: 35, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substr(1, 1000)}</Text>
                </View>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image style={{ height: 60, width: 60 }} source={ArrowBack} />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image style={{ height: 60, width: 60 }} source={ArrowNext} />
                </TouchableOpacity>
              </View>
            </Header>
          </View>
        }

        { mobile5 === true &&
          <View>
            <Header title='Мастера'>
              <Image source={img} style={{ position: 'fixed', left: '18%', bottom: 0, width: 230, height: 230 }} />

              <View style={{ position: 'fixed', top: '16.3vh', right: '5%', width: '70%' }}>
                <View className='stripe pink' />
              </View>

              <View style={{ position: 'fixed', top: '13vh', right: '5%' }}>
                <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ position: 'fixed', top: '17vh', right: '5%', width: win.width - 100 }} >
                <Text numberOfLines={1} ellipsizeMode={'tail'} style={{ flex: 1, fontFamily: 'CirceExtraLight', fontSize: 15, textAlign: 'right' }}>
                  {subTitle} 
                </Text>
              </View>

              <View style={{ position: 'fixed', width: '90%', top: '23vh', left: '5%' }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 10, lineHeight: 12 }}>
                    <Text style={{ fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substr(1, 1000)}
                  </Text>
                </View>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowBack}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
            </Header>
          </View>
        }
        </View>
    )
  }
}
