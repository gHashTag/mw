import React, { PureComponent } from 'react'
import { Dimensions, StyleSheet, View, Text } from 'react-native' // eslint-disable-line
import { Helmet } from 'react-helmet'
import LoadableVisibility from 'react-loadable-visibility/react-loadable'
import Logo from '../Logo'
import Player from '../common/Backgroundvideo/Player'
import We from '../We'
import ImageBack from '../ImageBack/index'
//import Events from '../Events'
import MouseScroller from '../common/MouseScroller'
import User from '../common/User'
//import Masters from '../Landing/Masters'
//import Slide from '../Landing/Slide'
import logo from '../img/logo.jpg'
import { responsive } from '../../constants'

const { mobile5, mobile6, tablet, desktop, desktopHD } = responsive

const Loading = () => <View />

const Banners = LoadableVisibility({ loader: () => import('../Banners'), loading: Loading, delay: 1000 })
const Subscribe = LoadableVisibility({ loader: () => import('../Subscribe'), loading: Loading, delay: 1000 })
const Masters = LoadableVisibility({ loader: () => import('../Landing/Masters'), loading: Loading, delay: 1000 })
const Slide = LoadableVisibility({ loader: () => import('../Landing/Slide'), loading: Loading, delay: 1000 })
const Contact = LoadableVisibility({ loader: () => import('../Contacts/Contact'), loading: Loading, delay: 1000 })

class Home extends PureComponent {
  render() {
    return (
      <div>
        <Helmet>
          <meta charSet='utf-8' />
          <title>City Retreat·Mansion Wellness</title>
          <link rel='canonical' href='https://cityretreat.ru/' />
          <meta property='og:image' content={logo} />
          <meta name='keywords' content='йога, сити ретрит, spa, wellness, спа, велнес' />
        </Helmet>
        { desktopHD && 
          <View>
            <User />
            <Player />
            <MouseScroller />
            <Logo />
            <Banners />
            <View style={[styles.page1, { zIndex: 3 }]}>
              <Masters />
            </View>
            <We />
            <View style={[styles.page1, { zIndex: 2 }]}>
              <Slide />
            </View>
            <Subscribe />
            <View style={[styles.page, { zIndex: 1 }]}>
              <Contact newheight={0} />
            </View>
          </View>
        }
        { desktop && 
          <View>
            <User />
            <Player />
            <MouseScroller />
            <Logo />
            <Banners />
            <View style={[styles.page1, { zIndex: 3 }]}>
              <Masters />
            </View>
            <We />
            <View style={[styles.page1, { zIndex: 2 }]}>
              <Slide />
            </View>
            <Subscribe />
            <View style={[styles.page, { zIndex: 1 }]}>
              <Contact newheight={0} />
            </View>
          </View>
        }
        { tablet && 
          <View>
            <User />
            <Player />
            <MouseScroller />
            <Logo />
            <Banners />
            <View style={[styles.page1, { zIndex: 3 }]}>
              <Masters />
            </View>
            <We />
            <View style={[styles.page1, { zIndex: 2 }]}>
              <Slide />
            </View>
            <Subscribe />
            <View style={[styles.page, { zIndex: 1 }]}>
              <Contact newheight={0} />
            </View>
          </View>
        }
        { mobile6 && 
          <View>
            <User />
            <ImageBack />
            <MouseScroller />
            <Logo />
            <Banners />
            <View style={[styles.page1, { zIndex: 3 }]}>
              <Masters />
            </View>
            <We />
            <View style={[styles.page1, { zIndex: 2 }]}>
              <Slide />
            </View>
            <Subscribe />
            <View style={[styles.page, { zIndex: 1 }]}>
              <Contact newheight={0} />
            </View>
          </View>
        }
        { mobile5 && 
          <View>
            <User />
            <ImageBack />
            <MouseScroller />
            <Logo />
            <Banners />
            <View style={[styles.page1, { zIndex: 3 }]}>
              <Masters />
            </View>
            <We />
            <View style={[styles.page1, { zIndex: 2 }]}>
              <Slide />
            </View>
            <Subscribe />
            <View style={[styles.page, { zIndex: 1 }]}>
              <Contact newheight={0} />
            </View>
          </View>
        }
      </div>
    )
  }
}

const styles = StyleSheet.create({
  page: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    shadowColor: '#9B9B9B',
    shadowOffset: { width: 0, height: 2.5 },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    backgroundColor: '#fff'
  },
  page1: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  h1: {
    flex: 1,
    color: '#4B4B4B',
    fontFamily: 'CirceLight',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: '100',
    textAlign: 'center'
  }
})

export default Home
