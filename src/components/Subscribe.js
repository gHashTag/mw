import React, { PureComponent } from 'react'
import { TextInput, CheckBox, View, Text, Dimensions, TouchableHighlight } from 'react-native'
import ReactHoverObserver from 'react-hover-observer'
import MailchimpSubscribe from 'react-mailchimp-subscribe'
import AOS from 'aos'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faCheckCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle'
import { ButtonUI, Spinner } from './common'
import { constants } from '../constants'
import './aos.css'
 
const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: 100,
    alignItems: 'center'
  },
  containerStyle: {
    flexDirection: mobile5 ? 'column' : mobile6 ? 'column' : tablet ? 'row' : desktop ? 'row' : '',
    paddingTop: 20,
    paddingBottom: 5
  },
  textStyle1: {
    fontFamily: 'CirceExtraLight',
    fontSize: mobile5 ? 35 : mobile6 ? 40 : tablet ? 80 : desktop ? 80 : '',
    fontStyle: 'normal',
    paddingBottom: 10,
    lineHeight: 'normal',
    letterSpacing: 10.3,
    textAlign: 'center',
    color: '#fff'
  },
  line: {
    width: mobile5 ? 100 : mobile6 ? 100 : tablet ? 100 : desktop ? 100 : '',
    height: 2,
    backgroundColor: '#fff',
    opacity: 0.73
  },
  textStyle2: {
    width: (win.width / 2) + 68,
    marginTop: 20,
    height: 246,
    opacity: 0.73,
    fontFamily: 'CirceLight',
    fontSize: mobile5 ? 12 : mobile6 ? 15 : tablet ? 16 : desktop ? 16 : '', 
    fontStyle: 'normal',
    lineHeight: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 16 : '', 
    textAlign: 'center',
    color: '#fff'
  },
  inputStyle: {
    backgroundColor: '#fff',
    height: mobile5 ? 27 : mobile6 ? 27 : tablet ? 30 : desktop ? 39 : '', 
    paddingRight: 5,
    paddingTop: 3,
    marginBottom: mobile5 ? 10 : mobile6 ? 10 : tablet ? 0 : desktop ? 0 : '',
    paddingLeft: 8,
    fontSize: mobile5 ? 13 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '', 
    lineHeight: 23,
    fontFamily: 'CirceLight',
    flex: 2
  },
  icons: {
    width: 100,
    height: 100,
    color: constants.SECONDARY,
    paddingTop: 2
  },
  check: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'center', 
    marginBottom: 200, 
    alignSelf: 'center'
  },
  textCheck: {
    fontFamily: constants.FONTLIGHT, 
    color: constants.OPACITY, 
    fontSize: mobile5 ? 13 : mobile6 ? 14 : tablet ? 15 : desktop ? 15 : '', 
    paddingLeft: 10 
  }
}

AOS.init()

const CustomForm = ({ disabled, status, message, onValidated }) => {
  const { containerStyle, inputStyle, icons } = styles
  let email;
  const submit = () =>
    email &&
    email.value.indexOf('@') > -1 &&
    onValidated({
      EMAIL: email.value
    })

  const _renderButton = () => {
    return (
      <View 
        data-aos='fade-up'
        data-aos-duration='2000' 
        data-aos-delay='100' 
        style={containerStyle}
      >
        <TextInput
          style={inputStyle}
          ref={node => (email = node)}
          type='email'
          placeholder='Email'
        />
        <View style={{ alignSelf: 'center' }}>
          <ReactHoverObserver>
            <ButtonUI 
              disabled={disabled}
              title='Подписаться' 
              onPress={submit} 
            />
          </ReactHoverObserver>
        </View>
      </View>
    )
  }

  return (
    <View style={containerStyle}>
      {status === 'sending' && <Spinner />}
      {status === 'error' && (
        <View style={containerStyle}>
          <div
            style={{ color: 'red' }}
            dangerouslySetInnerHTML={{ __html: message }}
          />
        </View>
      )}
      {status === 'success' && (
        <FontAwesomeIcon style={icons} icon={faCheckCircle} />
      )}
      {status !== 'success' && 
        _renderButton()
      }
    </View>
  )
}

export default class Subscribe extends PureComponent {
  state = { trueCheckBoxIsOn: true }

  renderCheck = () => {
    const { trueCheckBoxIsOn } = this.state
    return (
      <View data-aos='fade-up' data-aos-duration='2000' data-aos-delay='100' style={styles.check}>
        <CheckBox
          onValueChange={value => this.setState({trueCheckBoxIsOn: value})}
          color={constants.SECONDARY}
          value={trueCheckBoxIsOn}
        />
        <TouchableHighlight
          onPress={this.goToPrivacy}
          activeOpacity={0.3}
          underlayColor={constants.GOLDOPACITY} 
        >
          <Text style={styles.textCheck}>Принимаю соглашение сайта</Text>
        </TouchableHighlight>
      </View>
    )
  }

  render() {
    const { container, textStyle1, line } = styles
    const { trueCheckBoxIsOn } = this.state
    const url = 'https://cityretreat.us17.list-manage.com/subscribe/post?u=dafe0e71bb8517e17a47df30d&amp;id=2cd584d134'
    return (
      <View style={container}>
        <Text 
          data-aos='fade-up'
          data-aos-duration='2000' 
          data-aos-delay='200' 
          style={textStyle1}
        >НОВОСТИ</Text>
        <View 
          data-aos='fade-up'
          data-aos-duration='2000' 
          data-aos-delay='200' 
          style={line} 
        />
        <MailchimpSubscribe
          data-aos='fade-up'
          data-aos-duration='3000' 
          data-aos-delay='100' 
          url={url}
          render={({ subscribe, status, message }) => (
            <CustomForm
              status={status}
              message={message}
              disabled={!trueCheckBoxIsOn}
              onValidated={formData => subscribe(formData)}
            />
          )}
        />
        {this.renderCheck()}
      </View>
    )
  }
}

