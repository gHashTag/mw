import React, { PureComponent } from 'react'
import { View, Text, Dimensions } from 'react-native'
import AOS from 'aos'
import './aos.css'
 
const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w <= 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    height: 500,
    paddingTop: mobile5 ? 35 : mobile6 ? 40 : mobile6plus ? 0 : 80,
    alignItems: 'center'
  },
  textStyle1: {
    fontFamily: 'CirceExtraLight',
    fontSize: mobile5 ? 35 : mobile6 ? 40 : mobile6plus ? 40 : 80,
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 10.3,
    textAlign: 'center',
    color: '#fff'
  },
  line: {
    width: 100, 
    alignSelf: 'center',
    height: 2,
    backgroundColor: '#fff',
    opacity: 0.73
  },
  textStyle2: {
    width: (win.width / 2) + 68,
    marginTop: 20,
    height: 246,
    opacity: 0.73,
    fontFamily: 'CirceLight',
    fontSize: mobile5 ? 12 : mobile6 ? 15 : mobile6plus ? 15 : 16, 
    fontStyle: 'normal',
    //lineHeight: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 16 : '', 
    textAlign: 'center',
    color: '#fff'
  }
}

AOS.init()

export default class We extends PureComponent {
  render() {
    const { container, textStyle1, line, textStyle2 } = styles
    return (
      <View style={container}>
        { desktop === true &&
          <View>
            <Text data-aos='fade-up' data-aos-duration='1000' data-aos-delay='100' style={textStyle1} accessibilityRole='heading' >О НАС</Text>
            <View data-aos='fade-up' data-aos-duration='2000' data-aos-delay='100' style={line} />
            <Text data-aos='fade-up' data-aos-duration='1000' data-aos-delay='100' className='museo' style={textStyle2} accessibilityRole='heading' aria-level='2' >
              Особняк City Retreat Club Mansion Wellness в сердце Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
              Уникальный формат клуба – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
            </Text>
          </View>
        }
        { tablet === true &&
          <View>
            <Text data-aos='fade-up' data-aos-duration='1000' data-aos-delay='100' style={textStyle1} accessibilityRole='heading' >О НАС</Text>
            <View data-aos='fade-up' data-aos-duration='2000' data-aos-delay='100' style={line} />
            <Text data-aos='fade-up' data-aos-duration='1000' data-aos-delay='100' className='museo' style={textStyle2} accessibilityRole='heading' aria-level='2' >
              Особняк City Retreat Club Mansion Wellness в сердце Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
              Уникальный формат клуба – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
            </Text>
          </View>
        }
        { mobile6plus === true &&
          <View>
            <Text style={textStyle1} accessibilityRole='heading' >О НАС</Text>
            <View style={line} />
            <Text 
              className='museo'
              style={textStyle2}
              accessibilityRole='heading' aria-level='2'
            >
              Особняк City Retreat Club Mansion Wellness в сердце Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
              Уникальный формат клуба – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
            </Text>
          </View>
        }
        { mobile6 === true &&
          <View>
            <Text style={textStyle1} accessibilityRole='heading' >О НАС</Text>
            <View style={line} />
            <Text 
              className='museo'
              style={textStyle2}
              accessibilityRole='heading' aria-level='2'
            >
              Особняк City Retreat Club Mansion Wellness в сердце Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
              Уникальный формат клуба – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
            </Text>
          </View>
        }
        { mobile5 === true &&
          <View>
            <Text style={textStyle1} accessibilityRole='heading' >О НАС</Text>
            <View style={line} />
            <Text 
              className='museo'
              style={textStyle2}
              accessibilityRole='heading' aria-level='2'
            >
              Особняк City Retreat Club Mansion Wellness в сердце Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
              Уникальный формат клуба – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
            </Text>
          </View>
        }
      </View>
    )
  }
}

