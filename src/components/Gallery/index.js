import React from 'react'
import { Dimensions } from 'react-native'
import Gallery from './Gallery'
import { Header } from '../common'
import Door from '../common/Door'
import MouseScroller from '../common/MouseScroller'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default () => (
  <div>
    <Header title="Галерея">
      { desktopHD === true &&
        <Door height={2000}>
          <Gallery /> 
        </Door>
      }
      { desktop === true &&
        <Door height={2000}>
          <Gallery /> 
        </Door>
      }
      { tablet === true &&
        <Door height={2000}>
          <Gallery /> 
        </Door>
      }
      { mobile6plus === true &&
        <Gallery /> 
      }
      { mobile6 === true &&
        <Gallery /> 
      }
      { mobile5 === true &&
        <Gallery /> 
      }
    </Header>
    <MouseScroller />
  </div>
)
