import g0 from './images/g0.png'
import g1 from './images/g1.png'
import g2 from './images/g2.png'
import g3 from './images/g3.png'
import g4 from './images/g4.png'
import g5 from './images/g5.png'
import g6 from './images/g6.png'
import g7 from './images/g7.png'
import g8 from './images/g8.png'
import g9 from './images/g9.png'
import g10 from './images/g10.png'
import g11 from './images/g11.png'
import g12 from './images/g12.png'
import g13 from './images/g13.png'
import g14 from './images/g14.png'
import g15 from './images/g15.png'
import g16 from './images/g16.png'
import g17 from './images/g17.png'
import g19 from './images/g19.png'
import g20 from './images/g20.png'
import g21 from './images/g21.png'
import g22 from './images/g22.png'

export const data = [
  {
    id: 0,
    img: g0 
  },
  {
    id: 1,
    img: g1 
  },
  {
    id: 2,
    img: g3 
  },
  {
    id: 3,
    img: g4 
  },
  {
    id: 4,
    img: g5 
  },
  {
    id: 5,
    img: g6 
  },
  {
    id: 6,
    img: g7 
  },
  {
    id: 7,
    img: g8 
  },
  {
    id: 8,
    img: g9 
  },
  {
    id: 9,
    img: g10 
  },
  {
    id: 10,
    img: g11 
  },
  {
    id: 11,
    img: g12 
  },
  {
    id: 12,
    img: g13 
  },
  {
    id: 13,
    img: g14 
  },
  {
    id: 14,
    img: g15
  },
  {
    id: 15,
    img: g16 
  },
  {
    id: 16,
    img: g17
  },
  {
    id: 17,
    img: g19 
  },
  {
    id: 18,
    img: g20 
  },
  {
    id: 19,
    img: g21 
  },
  {
    id: 20,
    img: g22 
  },
  {
    id: 22,
    img: g2 
  }
]
