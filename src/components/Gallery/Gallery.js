import React, { PureComponent } from 'react'
import { ScrollView, Dimensions, Image, View } from 'react-native' 
import { Helmet } from 'react-helmet'
import { data } from './data.js'
import g0 from './images/g0.png'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Gallery extends PureComponent {
  render() {
    const { container } = styles
    return (
      <View>
        <Helmet>
          <meta charSet='utf-8' />
          <title>Галерея·Mansion Wellness</title>
          <link rel='canonical' href='http://cityretreat.ru/gallery/0' />
          <meta property='og:image' content={g0} />
          <meta name='keywords' content='йога, сити ретрит, spa, wellness, спа, велнес' />
          <meta name='description' content='Особняк в центре Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы. Уникальный формат Центра – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.' />
        </Helmet>
        { desktop === true &&
          <View style={[container, { marginTop: h + 200, marginBottom: 10 }]}>
            <ScrollView horizontal contentContainerStyle={{ paddingTop: '0vh', paddingBottom: 20 }} scrollEventThrottle={16} style={{ paddingBottom: 20 }} >
              {data.map(({ id, img }) => (
                <View key={id} style={{ paddingRight: 1 }}>
                  <img src={img} style={{ height: 400, width: 530 }} alt='City Retreat' />
                </View>
              ))}
            </ScrollView>
          </View>
        }

        { tablet === true &&
          <View style={[container, { marginTop: h + 350, marginBottom: 15 }]}>
            <ScrollView horizontal contentContainerStyle={{ paddingTop: '20vh' }} scrollEventThrottle={16}>
              {data.map(({ id, img }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingRight: 1 }} >
                  <img src={img} style={{ height: win.width * 0.66, width: win.width - 90 }} alt='City Retreat' />
                </View>
              ))}
            </ScrollView>
          </View>
        }

        { mobile6 === true &&
          <View style={[container, { marginTop: 90, marginBottom: 200 }]}>
            <View contentContainerStyle={{ paddingTop: '20vh' }}>
              {data.map(({ id, img }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingBottom: 10, paddingRight: 1 }} >
                  <Image source={img} style={{ height: win.width * 0.66, width: win.width - 40 }} />
                </View>
              ))}
            </View>
          </View>
        }

        { mobile5 === true &&
          <View style={[container, { marginTop: 60, marginBottom: 200 }]}>
            <View contentContainerStyle={{ paddingTop: '20vh', paddingBottom: 20 }}>
              {data.map(({ id, img }) => (
                <View key={id} style={{ flexGrow: 1, paddingTop: 5, justifyContent: 'center', paddingRight: 1 }} >
                  <Image source={img} style={{ height: win.width * 0.66, width: win.width - 40 }} />
                </View>
              ))}
            </View>
          </View>
        }
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
    marginBottom: 40
  },
  headerView: {
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center'
  },
  header: {
    alignItems: 'center',
    fontFamily: 'Museo500', 
    fontSize: 35 
  },
  description: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'CirceLight', 
    marginLeft: 20,
    marginRight: 20,
    fontSize: 15 
  }
}

export default Gallery 

