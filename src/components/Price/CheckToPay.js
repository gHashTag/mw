import React from 'react'
import { AsyncStorage } from 'react-native'
import shortid from 'shortid'
import sha1 from 'js-sha1'
import { withApollo, compose, graphql } from 'react-apollo'
import { connect } from 'react-redux'
import gql from 'graphql-tag'
import { Loading } from '../common' 
import { TOKEN_KEY, constants } from '../../constants'

class CheckToPay extends React.Component {
  state = { error: '' }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem(TOKEN_KEY)
    if (!token) {
      this.props.history.push('/signup')
      return
    }

    let response
    try {
      response = await this.props.mutate()
    } catch (err) {
      this.props.history.push('/signup')
      const error = this.props.data.error.message
      this.setState({ error })
      return
    }

    const { refreshToken: { token: newToken } } = response.data
    await AsyncStorage.setItem(TOKEN_KEY, newToken)
    const { order_desc, amount } = this.props.history.location.state
    this.getPayRequest({ order_desc, amount })
  }

  getPayRequest = async ({ order_desc, amount }) => {
    try {
      const payment_key = constants.MERCHANT_KEY 
      const currency = 'RUB'
      const merchant_id = constants.MERCHANT_ID 
      const order_id = shortid.generate()
      const { Email } = this.props.clientData
      const sender_email = Email 
      const body = {
        request: {
          order_id,
          order_desc,
          currency,
          amount,
          merchant_id,
          sender_email,
          signature: sha1(`${payment_key}|${amount}|${currency}|${merchant_id}|${order_desc}|${order_id}|${sender_email}`)
        }
      }
      const res = await fetch(constants.URLFONDY, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const data = await res.json()
      window.location.href = data.response.checkout_url 
    } catch (e) {
      throw e
    }
  }

  render() {
    return <Loading /> 
  }
}

const refreshTokenMutation = gql`
  mutation {
    refreshToken {
      token
    }
  }
`

const mapStateToProps = state => {
  return { 
    data: state.payData,
    clientData: state.me.clientData
  }
}

export default withApollo(compose(
  connect(mapStateToProps, null),
  graphql(refreshTokenMutation)
)(CheckToPay))

