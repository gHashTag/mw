import React, { PureComponent } from 'react'
import { ScrollView, TouchableHighlight, Linking, Dimensions, Image, View, Text } from 'react-native' // eslint-disable-line
import PriceCard from '../common/PriceCard'
import data1 from './data1.json'
import data2 from './data2.json'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const sale = 'Специальное предложение CITY RETREAT CLUB весна / лето 2018. Действует с 9.04 по 31.08 2018 г. включительно.'
const sale1 = 'В стоимость пакета включено посещение бассейна, сауны и хаммама.'

export default class Price extends PureComponent {
  render() {
    const { container, headerView, header, description } = styles
    return (
      <View>
        { desktop === true &&
          <View>
            <View style={[{ marginTop: h }, headerView]}>
              <Text style={header}>Разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 40 }]}>
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
              <Text style={description}>{sale1}</Text>
            </View>
            <View style={[{ marginTop: 120 }, headerView]}>
              <Text style={header}>Клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 200 }]}>
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
          </View>
        }

        { tablet === true &&
          <View>
            <View style={[{ marginTop: h }, headerView]}>
              <Text style={header}>Разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 50 }]}>
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
              <Text style={description}>{sale1}</Text>
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 90 }]}>
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
          </View>
        }

        { mobile6 === true &&
          <View>
            <View style={[{ marginTop: 120 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
              <Text style={description}>{sale1}</Text>
            </View>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingBottom: 100 }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
          </View>
        }

        { mobile5 === true &&
          <View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
              <Text style={description}>{sale1}</Text>
            </View>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingBottom: 100 }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
          </View>
        }
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
    marginBottom: 40
  },
  headerView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    alignItems: 'center',
    fontFamily: 'Museo500', 
    fontSize: 35 
  },
  description: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'CirceLight', 
    marginLeft: 20,
    marginRight: 20,
    fontSize: 15 
  }
}
