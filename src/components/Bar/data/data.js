import b0 from '../images/0.png'
import b1 from '../images/1.png'
import b2 from '../images/2.png'
import b3 from '../images/3.png'
import b4 from '../images/4.png'
import b5 from '../images/5.png'
import b6 from '../images/6.png'
import b7 from '../images/7.png'
import b8 from '../images/8.png'

export const data = [
  {
    id: 0,
    name: '0',
    img: b0
  },
  {
    id: 1,
    name: '1',
    img: b1
  },
  {
    id: 2,
    name: '2',
    img: b2
  },
  {
    id: 3,
    name: '3',
    img: b3
  },
  {
    id: 4,
    name: '4',
    img: b4
  },
  {
    id: 5,
    name: '5',
    img: b5
  },
  {
    id: 6,
    name: '6',
    img: b6
  },
  {
    id: 7,
    name: '7',
    img: b7
  },
  {
    id: 8,
    name: '8',
    img: b8
  }
]
