import React from 'react'
import { Dimensions } from 'react-native'
import { Header } from '../common'
import Bar from './Bar'
import Door from '../common/Door'
import MouseScroller from '../common/MouseScroller'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default () => (
  <div>
    <Header title='Бар'>
      { desktopHD === true &&
        <Door height={2000}>
          <Bar /> 
        </Door>
      }
      { desktop === true &&
        <Door height={2000}>
          <Bar /> 
        </Door>
      }
      { tablet === true &&
        <Door height={2000}>
          <Bar /> 
        </Door>
      }
      { mobile6plus === true &&
        <Bar /> 
      }
      { mobile6 === true &&
        <Bar /> 
      }
      { mobile5 === true &&
        <Bar /> 
      }
    </Header>
    <MouseScroller />
  </div>
)
