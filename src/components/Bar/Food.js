import React, { PureComponent } from 'react'
import { Dimensions, View, Text } from 'react-native' 

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Food extends PureComponent {
  render() {
    const { data } = this.props
    const { h1, h2 } = styles
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 0, paddingBottom: 10 }}>
        { desktop === true &&
          <View style={{ marginTop: 20, marginBottom: 30 }}>
            <Text style={[h1, { fontSize: 40, textAlign: 'right' }]} >
              {data.name}
            </Text>
          </View>
        }
        { tablet === true &&
          <View style={{ marginTop: 20, marginBottom: 25 }}>
            <Text style={[h1, { fontSize: 30, textAlign: 'right' }]} >
              {data.name}
            </Text>
          </View>
        }
        { mobile6 === true &&
          <View style={{ marginTop: 10, marginBottom: 10 }}>
            <Text style={[h1, { fontSize: 24, textAlign: 'right' }]} >
              {data.name}
            </Text>
          </View>
        }
        { mobile5 === true &&
          <View style={{ marginTop: 10, marginBottom: 10 }}>
            <Text style={{ fontFamily: 'Museo500', fontSize: 18, textAlign: 'right' }} >
              {data.name}
            </Text>
          </View>
        }

        { desktop === true &&
          <View>
            {data.items.map(({ id, item, price }) => (
              <View key={id} style={{ flexDirection: 'row', justifyContent: 'space-between', width: 700, paddingRight: 50, paddingBottom: 10 }}>
                <Text style={[h2, { fontSize: 22, justifyContent: 'flex-start', paddingLeft: 50, paddingRight: 20 }]}>{item}</Text>
                <View>
                  <Text style={[h2, { fontSize: 22 }]}>{price} {'\u20bd'}</Text>
                </View>
              </View>
            ))}
          </View>
        }
        { tablet === true &&
          <View>
            {data.items.map(({ id, item, price }) => (
              <View key={id} style={{ flexDirection: 'row', justifyContent: 'space-between', width: 700, paddingRight: 50, paddingBottom: 10 }}>
                <Text style={[h2, { fontSize: 22, justifyContent: 'flex-start', paddingLeft: 50, paddingRight: 20 }]}>{item}</Text>
                <View>
                  <Text style={[h2, { fontSize: 22 }]}>{price} {'\u20bd'}</Text>
                </View>
              </View>
            ))}
          </View>
        }
        { mobile6 === true &&
          <View>
            {data.items.map(({ id, item, price }) => (
              <View key={id} style={{ flexDirection: 'row', justifyContent: 'space-between', width: 420, paddingRight: 60, paddingBottom: 5 }}>
                <Text style={[h2, { fontSize: 13, justifyContent: 'flex-start', paddingLeft: 50, paddingRight: 10 }]}>{item}</Text>
                <View>
                  <Text style={[h2, { fontSize: 13 }]}>{price} {'\u20bd'}</Text>
                </View>
              </View>
            ))}
          </View>
        }
        { mobile5 === true &&
          <View>
            {data.items.map(({ id, item, price }) => (
              <View key={id} style={{ flexDirection: 'row', justifyContent: 'space-between', width: 380, paddingRight: 50, paddingBottom: 5 }}>
                <Text style={[h2, { fontSize: 12, justifyContent: 'flex-start', paddingLeft: 50, paddingRight: 10 }]}>{item}</Text>
                <View>
                  <Text style={[h2, { fontSize: 12 }]}>{price} {'\u20bd'}</Text>
                </View>
              </View>
            ))}
          </View>
        }
      </View>
    )
  }
}

const styles = {
  h1: {
    fontFamily: 'Museo500' 
  },
  digits: {
    fontFamily: 'Arnamu'
  },
  h2: {
    fontFamily: 'CirceExtraLight' 
  }
}

export default Food
