import React, { PureComponent } from 'react'
import { View, Dimensions, ScrollView, Image } from 'react-native'
import Food from './Food'
import { Helmet } from 'react-helmet'
import { data } from './data/data'
import dataBreakfast from './data/dataBreakfast.json'
import dataSoup from './data/dataSoup.json'
import dataBruschettes from './data/dataBruschettes.json'
import dataDessert from './data/dataDessert.json'
import dataCoffee from './data/dataCoffee.json'
import dataTea from './data/dataTea.json'
import dataSmoothies from './data/dataSmoothies.json'
import dataDetox from './data/dataDetox.json'
import dataFresh from './data/dataFresh.json'
import dataDrinks from './data/dataDrinks.json'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Bar extends PureComponent {
  render() {
    const { container } = styles
    return (
      <View>
        <Helmet>
          <meta charSet='utf-8' />
          <title>Эко бар·Mansion Wellness</title>
          <link rel='canonical' href='http://cityretreat.ru/bar' />
          <meta name='keywords' content='йога, сити ретрит, spa, wellness, бар, велнес' />
          <meta name='description' content='Особняк в центре Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы. Уникальный формат Центра – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.' />
        </Helmet>
        <View>
          { desktop === true &&
            <View style={[container, { marginTop: h + 200, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '10vh', paddingBottom: 20 }} scrollEventThrottle={16} style={{ padding: 20 }} >
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingRight: 1 }} >
                    <Image source={img} style={{ height: 300, width: 400 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
          }
          { tablet === true &&
            <View style={[container, { marginTop: h + 350, marginBottom: 15 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '7vh' }} scrollEventThrottle={16} style={{ padding: 20 }} >
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingRight: 1 }} >
                    <Image source={img} style={{ height: 300, width: 400 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
          }
          { mobile6 === true &&
            <View style={[container, { marginTop: 60, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '7vh' }} scrollEventThrottle={16}>
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingBottom: 10 }} >
                    <Image source={img} style={{ height: 300, width: 400 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
          }
          { mobile5 === true &&
            <View style={[container, { marginTop: 60, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '7vh', paddingBottom: 10 }} scrollEventThrottle={16}>
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingRight: 1 }} >
                    <Image source={img} style={{ height: 300, width: 400 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
          }
          <View style={{ paddingBottom: 300 }}>
            <Food data={dataBreakfast} />
            <Food data={dataSoup} />
            <View>
              <Food data={dataBruschettes} />
            </View>
            <View>
              <Food data={dataDessert} />
            </View>
            <View>
              <Food data={dataCoffee} />
            </View>
            <View>
              <Food data={dataTea} />
            </View>
            <View>
              <Food data={dataSmoothies} />
            </View>
            <View>
              <Food data={dataDetox} />
            </View>
            <View>
              <Food data={dataFresh} />
            </View>
            <View>
              <Food data={dataDrinks} />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
    marginBottom: 40
  },
  headerView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    alignItems: 'center',
    fontFamily: 'Museo500', 
    fontSize: 35 
  },
  description: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'CirceLight', 
    marginLeft: 20,
    marginRight: 20,
    fontSize: 15 
  }
}

export default Bar 
