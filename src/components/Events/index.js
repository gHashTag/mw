import React from 'react'
import { Dimensions } from 'react-native'
import Events from './Events'
import Door from '../common/Door'
import MouseScroller from '../common/MouseScroller'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 341 && w < 376
const mobile6plus = w > 375 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

export default () => (
  <div>
    { desktopHD === true &&
      <Door height={1500}>
        <Events />
      </Door>
    }
    { desktop === true &&
      <Door height={1500}>
        <Events />
      </Door>
    }
    { tablet === true &&
      <Door height={1500}>
        <Events />
      </Door>
    }
    { mobile6plus === true &&
      <Events />
    }
    { mobile6 === true &&
      <Events />
    }
    { mobile5 === true &&
      <Events />
    }
    <MouseScroller />
  </div>
)
