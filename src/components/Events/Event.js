import React, { PureComponent } from 'react' 
import { Linking, TouchableOpacity, Dimensions, Image, View, Text } from 'react-native' // eslint-disable-line
import { connect } from 'react-redux'
import ReactHoverObserver from 'react-hover-observer'
import { Helmet } from 'react-helmet'
import { find } from 'lodash'
import { withRouter } from 'react-router'
import { clearStore } from '../../actions'
import './styles.css'
import { Header, ButtonUI } from '../common'
import CallToAction from '../common/CallToAction'
import ArrowNext from '../ArrowNext.png'
import ArrowBack from '../ArrowBack.png'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Event extends PureComponent {
  onPressNext = match => {
    const { data } = this.props
    return () => {
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (data.length !== now.id + 1) {
        const next = find(data, { id: now.id + 1 })
        history.push(`/events/${next.name}`)
        this.props.clearStore()
      } else {
        history.push(`/events/${data[0].name}`)
        this.props.clearStore()
      }
    }
  }

  onPressBack = match => {
    return () => {
      const { data } = this.props
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (now.id !== 0) {
        const back = find(data, { id: now.id - 1 })
        history.push(`/events/${back.name}`)
        this.props.clearStore()
      } else {
        const lengthdata = find(data, { id: data.length - 1 })
        history.push(`/events/${lengthdata.name}`)
        this.props.clearStore()
      }
    }
  }

  render() {
    const { match, data } = this.props
    const store = find(data, { name: match.params.name })
    const { title, subtitle, StartDate, img, info, url } = store
    const site = constants.SITE + match.url
    return (
      <View>
        <Helmet>
          <meta charSet='utf-8' />
          <title>{title}·City Retreat</title>
          <meta name='keywords' content={title} />
          <meta property='og:image' content={img} />
          <link rel='canonical' href={site} />
          <meta name='description' content={info} />
        </Helmet>
        <Header title="События">
          { desktop === true &&
            <View>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={img} style={{ marginTop: h + 200, height: win.width / 3.7, width: win.width / 2 }} />
              </View>

              <View style={{ flex: 1, marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 50 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { url !== '' &&
                <View style={{ flex: 1, paddingBottom: 50, justifyContent: 'center', alignItems: 'center' }}>
                  <ReactHoverObserver>
                    <ButtonUI title='Билеты' onPress={() => Linking.openURL(url)} />
                  </ReactHoverObserver>
                </View>
              }
              <CallToAction title={title} subtitle={subtitle} StartDate={StartDate} /> 

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 80, width: 80 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 80, width: 80 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </View>
          }

          { tablet === true &&
            <View>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={img} style={{ marginTop: h + 200, height: win.width / 3.8, width: win.width / 2 }} />
              </View>

              <View style={{ flex: 1, marginTop: 30, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 200 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
                { url !== '' &&
                  <View style={{ flex: 1, paddingBottom: 100, alignItems: 'center' }}>
                    <ReactHoverObserver>
                      <ButtonUI title='Билет' onPress={() => Linking.openURL(url)} />
                    </ReactHoverObserver>
                  </View>
                }
                <CallToAction title={title} subtitle={subtitle} StartDate={StartDate} /> 
              </View>

              <View style={{ position: 'fixed', width: '7%', top: '38%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ position: 'fixed', width: '7%', top: '38%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowBack}
                  />
                </TouchableOpacity>
              </View>
            </View>
          }

          { mobile6 === true &&
            <View>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={img} style={{ marginTop: 90, height: win.width / 1.8, width: win.width }} />
              </View>

              <View style={{ position: 'absolute', width: '90%', top: '49vh', left: '5%' }} >
                <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 50 }}>
                  <Text style={{ fontFamily: 'CirceExtraLight', fontSize: 15, lineHeight: 20 }}>
                    <Text style={{ fontFamily: 'Museo500', fontSize: 35, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
                { url !== '' &&
                  <View style={{ flex: 1, paddingBottom: 100, alignItems: 'center' }}>
                    <ReactHoverObserver>
                      <ButtonUI title='Билет' onPress={() => Linking.openURL(url)} />
                    </ReactHoverObserver>
                  </View>
                }
                <CallToAction title={title} subtitle={subtitle} StartDate={StartDate} /> 
              </View>

              <View style={{ position: 'fixed', bottom: '20%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image style={{ height: 50, width: 50 }} source={ArrowBack} />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image style={{ height: 50, width: 50 }} source={ArrowNext} />
                </TouchableOpacity>
              </View>
            </View>
          }

          { mobile5 === true &&
            <View>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={img} style={{ marginTop: 60, height: 180, width: win.width }} />
              </View>

              <View style={{ position: 'absolute', width: '90%', top: '48vh', left: '5%' }} >
                <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 50 }}>
                  <Text style={{ fontFamily: 'CirceLight', fontSize: 10, lineHeight: 12 }}>
                    <Text style={{ fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
                { url !== '' &&
                  <View style={{ flex: 1, paddingBottom: 100, alignItems: 'center' }}>
                    <ReactHoverObserver>
                      <ButtonUI title='Билет' onPress={() => Linking.openURL(url)} />
                    </ReactHoverObserver>
                  </View>
                }
                <CallToAction title={title} subtitle={subtitle} StartDate={StartDate} /> 
              </View>

              <View style={{ position: 'fixed', bottom: '20%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image
                    style={{ height: 50, width: 50 }}
                    source={ArrowBack}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '20%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 50, width: 50 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
            </View>
          }
        </Header>
      </View>
    )
  }
}

export default withRouter(connect(null, { clearStore })(Event))
