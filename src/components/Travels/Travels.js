import React, { PureComponent } from 'react'
import { TouchableOpacity, TouchableHighlight, Dimensions, Image, Button, View, ProgressBar, Text } from 'react-native' // eslint-disable-line
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'
import { stack as Menu } from 'react-burger-menu'
import Travel from './Travel'
import { data } from './data'
import Dot from './dotdotdot.svg'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const platform = (x) => {
  try {
    if (w <= 320) {
      return x + 3 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x * 3.7 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x * 3.9 // 'iphone6+'
    } else if (w > 414) {
      return x * 3.9 // 'web'
    }
  } catch (e) {
    throw e
  }
}

const menuY = (x) => {
  try {
    if (w <= 320) {
      return x + 2 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x + 3 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x + 3 //'iphone6+'
    } else if (w > 414) {
      return x + 20// 'web'
    }
  } catch (e) {
    throw e
  }
}

const styles = {
  bmBurgerButton: {
    position: 'fixed',
    width: '43px',
    height: '53px',
    right: platform(5) + 9,
    top: menuY(1),
    zIndex: 6
  },
  bmCrossButton: {
    position: 'absolute',
    left: '10px',
    height: '24px',
    width: '24px'
  },
  bmCross: {
    background: '#fff'
  },
  bmMenu: {
    background: 'rgba(74, 74, 74, 0.70)',
    fontFamily: 'CirceLight',
    fontWeight: '500',
    fontSize: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 16 : '' 
  },
  bmMorphShape: {
    fill: '#373a47'
  },
  bmItemList: {
    color: '#fff',
    marginTop: mobile5 ? 20 : mobile6 ? 30 : tablet ? 80 : desktop ? 80 : '', 
    marginLeft: 50
  },
  bmOverlay: {
    background: 'rgba(0, 0, 0, 0.6)'
  },
  navLink: { 
    color: '#fff', 
    textDecoration: 'none',
    paddingLeft: 10
  },
  activeStyle: {
    color: '#D6B784' 
  },
  link: {
    textDecoration: 'none'
  }
}

class Travels extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      menuOpen: false
    }
  }

  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen })  
  }

  closeMenu() {
    this.setState({ menuOpen: false })
  }

  toggleMenu() {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  render() {
    const { navLink, activeStyle } = styles
    return (
      <Router>
        <div>
          <Menu 
            isOpen={this.state.menuOpen}
            onStateChange={(state) => this.handleStateChange(state)}
            styles={styles} 
            right
            customBurgerIcon={!this.state.menuOpen ? <Image source={Dot} /> : false} 
          >
            {data.map(({ name, id, title }) => (
              <div key={id}>
                <NavLink 
                  onClick={() => this.closeMenu()}
                  to={`/travels/${name}`} 
                  style={navLink} 
                  activeStyle={activeStyle}
                >
                  {title}
                </NavLink>
              </div>
            ))}
          </Menu>
          <Route path="/travels/:name" component={Travel} />
        </div>
      </Router>
    )
  }
}

export default Travels 
