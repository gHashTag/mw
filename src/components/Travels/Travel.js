import React, { PureComponent } from 'react' 
import { TouchableOpacity, Dimensions, Image, View, Text } from 'react-native' 
import { withRouter } from 'react-router'
import { find } from 'lodash'
import { Header } from '../common'
import { data } from './data.js'
import './styles.css'
import ArrowNext from '../ArrowNext.png'
import ArrowBack from '../ArrowBack.png'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 
const desktopHD = w >= 1920

class Index extends PureComponent {
  onPressNext = match => {
    return () => {
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (data.length !== now.id + 1) {
        const next = find(data, { id: now.id + 1 })
        history.push(`/travels/${next.name}`)
      } else {
        history.push(`/travels/${data[0].name}`)
      }
    }
  }

  onPressBack = match => {
    return () => {
      const now = find(data, { name: match.params.name })
      const { history } = this.props
      if (now.id !== 0) {
        const back = find(data, { id: now.id - 1 })
        history.push(`/travels/${back.name}`)
      } else {
        const lengthdata = find(data, { id: data.length - 1 })
        history.push(`/travels/${lengthdata.name}`)
      }
    }
  }

  render() {
    const { match } = this.props
    const store = find(data, { name: match.params.name })
    const { title, title1, title2, title3, title4, title5, title6, img, img1, img2, img3, img4, img5, img6, info, info1, info2, info3, info4, info5, info6 } = store

    return (
      <View>
        { desktopHD === true &&
          <View>
            <Header title='Путешествия'>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 150 }}>
                <Text style={{ marginTop: h, fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                <Image source={img} style={{height: win.width / 3.6, width: win.width / 2 }} />
              </View>

              <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { title1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title1} 
                  </Text>
                </View>
              }

              { img1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img1} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info1 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info1.charAt(0)}
                      </Text>
                      {info1.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title2} 
                  </Text>
                </View>
              }

              { img2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img2} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info2 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info2.charAt(0)}
                      </Text>
                      {info2.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title3} 
                  </Text>
                </View>
              }

              { img3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img3} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info3 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info3.charAt(0)}
                      </Text>
                      {info3.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title4} 
                  </Text>
                </View>
              }

              { img4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info4 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info4.charAt(0)}
                      </Text>
                      {info4.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title5} 
                  </Text>
                </View>
              }

              { img5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info5 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info5.charAt(0)}
                      </Text>
                      {info5.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title6} 
                  </Text>
                </View>
              }

              { img6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img6} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info6 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 200 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 20, lineHeight: 25 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 50, lineHeight: 1, bottom: 0 }}>
                        {info6.charAt(0)}
                      </Text>
                      {info6.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 100, width: 100 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 100, width: 100 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { desktop === true &&
          <View>
            <Header title='Путешествия'>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 140 }}>
                <Text style={{ marginTop: h, fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                <Image source={img} style={{ height: win.width / 3.6, width: win.width / 2 }} />
              </View>

              <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { title1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title1} 
                  </Text>
                </View>
              }

              { img1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img1} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info1 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info1.charAt(0)}
                      </Text>
                      {info1.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title2} 
                  </Text>
                </View>
              }

              { img2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img2} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info2 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info2.charAt(0)}
                      </Text>
                      {info2.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title3} 
                  </Text>
                </View>
              }

              { img3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img3} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info3 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info3.charAt(0)}
                      </Text>
                      {info3.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title4} 
                  </Text>
                </View>
              }

              { img4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info4 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info4.charAt(0)}
                      </Text>
                      {info4.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title5} 
                  </Text>
                </View>
              }

              { img5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info5 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info5.charAt(0)}
                      </Text>
                      {info5.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 60, textAlign: 'right' }}>
                    {title6} 
                  </Text>
                </View>
              }

              { img6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 0 }}>
                  <Image source={img6} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info6 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 200 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 17, lineHeight: 20 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 40, lineHeight: 1, bottom: 0 }}>
                        {info6.charAt(0)}
                      </Text>
                      {info6.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 100, width: 100 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 100, width: 100 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { tablet === true &&
          <View>
            <Header title='Путешествия'>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 80 }}>
                <Text style={{ marginTop: h, fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                <Image source={img} style={{ height: win.width / 3.6, width: win.width / 2 }} />
              </View>

              <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { title1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title1} 
                  </Text>
                </View>
              }

              { img1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img1} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info1 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info1.charAt(0)}
                      </Text>
                      {info1.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title2} 
                  </Text>
                </View>
              }

              { img2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img2} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info2 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info2.charAt(0)}
                      </Text>
                      {info2.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title3} 
                  </Text>
                </View>
              }

              { img3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img3} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info3 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info3.charAt(0)}
                      </Text>
                      {info3.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title4} 
                  </Text>
                </View>
              }

              { img4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info4 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info4.charAt(0)}
                      </Text>
                      {info4.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title5} 
                  </Text>
                </View>
              }

              { img5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img4} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info5 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info5.charAt(0)}
                      </Text>
                      {info5.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 40, textAlign: 'right' }}>
                    {title6} 
                  </Text>
                </View>
              }

              { img6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginBottom: 60 }}>
                  <Image source={img6} style={{ height: win.width / 3.6, width: win.width / 2 }} />
                </View>
              }

              { info6 !== '' &&
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: win.width / 2, paddingBottom: 200 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 14, lineHeight: 18 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30, lineHeight: 1, bottom: 0 }}>
                        {info6.charAt(0)}
                      </Text>
                      {info6.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              <View style={{ position: 'fixed', width: '7%', top: '43%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 50, width: 50 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ position: 'fixed', width: '7%', top: '43%', left: '5%' }} > 
                { parseInt(match.params.id, 10) !== 0 &&
                  <TouchableOpacity onPress={this.onPressBack(match)}>
                    <Image
                      style={{ height: 50, width: 50 }}
                      source={ArrowBack}
                    />
                  </TouchableOpacity>
                }
              </View>
            </Header>
          </View>
        }

        { mobile6 === true &&
          <View>
            <Header title='Путешествия'>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 90 }}>
                <Text style={{ marginTop: 0, fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                <Image source={img} style={{ marginTop: 10, height: w * 0.57, width: w }} />
              </View>

              <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { title1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title1} 
                  </Text>
                </View>
              }

              { img1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img1} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info1 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info1.charAt(0)}
                      </Text>
                      {info1.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title2} 
                  </Text>
                </View>
              }

              { img2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img2} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info2 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info2.charAt(0)}
                      </Text>
                      {info2.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title3} 
                  </Text>
                </View>
              }

              { img3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img3} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info3 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info3.charAt(0)}
                      </Text>
                      {info3.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title4} 
                  </Text>
                </View>
              }

              { img4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img4} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info4 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info4.charAt(0)}
                      </Text>
                      {info4.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title5} 
                  </Text>
                </View>
              }

              { img5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img5} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info5 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info5.charAt(0)}
                      </Text>
                      {info5.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 26, textAlign: 'right' }}>
                    {title6} 
                  </Text>
                </View>
              }

              { img6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img6} style={{ marginTop: 10, height: w * 0.57, width: w }} />
                </View>
              }

              { info6 !== '' &&
                <View style={{ marginTop: 10, justifyContent: 'center', alignItems: 'center', width: 345, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 200 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 15 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 35 }}>
                        {info6.charAt(0)}
                      </Text>
                      {info6.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              <View style={{ position: 'fixed', bottom: '15%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowBack}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '15%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
            </Header>
          </View>
        }

        { mobile5 === true &&
          <View>
            <Header title='Путешествия'>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 80 }}>
                <Text style={{ marginTop: 0, fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }} >
                  {title} 
                </Text>
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                <Image source={img} style={{ marginTop: 10, height: 180, width: win.width }} />
              </View>

              <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                  <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                    <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                      {info.charAt(0)}
                    </Text>
                    {info.substring(1)}
                  </Text>
                </View>
              </View>

              { title1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title1} 
                  </Text>
                </View>
              }

              { img1 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img1} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info1 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info1.charAt(0)}
                      </Text>
                      {info1.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title2} 
                  </Text>
                </View>
              }

              { img2 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img2} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info2 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info2.charAt(0)}
                      </Text>
                      {info2.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title3} 
                  </Text>
                </View>
              }

              { img3 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img3} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info3 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info3.charAt(0)}
                      </Text>
                      {info3.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title4} 
                  </Text>
                </View>
              }

              { img4 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img4} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info4 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info4.charAt(0)}
                      </Text>
                      {info4.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title5} 
                  </Text>
                </View>
              }

              { img5 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img2} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info5 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 0 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info5.charAt(0)}
                      </Text>
                      {info5.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              { title6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <Text style={{ fontFamily: 'Museo500', fontSize: 20, textAlign: 'right' }}>
                    {title6} 
                  </Text>
                </View>
              }

              { img6 !== '' &&
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 0, marginBottom: 0 }}>
                  <Image source={img6} style={{ marginTop: 10, height: 180, width: win.width }} />
                </View>
              }

              { info6 !== '' &&
                <View style={{ marginTop: 0, justifyContent: 'center', alignItems: 'center', width: 280, marginLeft: 20 }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingBottom: 200 }} >
                    <Text style={{ color: '#707070', fontFamily: 'CirceExtraLight', fontSize: 13 }}>
                      <Text style={{ color: '#000', fontFamily: 'Museo500', fontSize: 30 }}>
                        {info6.charAt(0)}
                      </Text>
                      {info6.substring(1)}
                    </Text>
                  </View>
                </View>
              }

              <View style={{ position: 'fixed', bottom: '15%', left: '5%' }} > 
                <TouchableOpacity onPress={this.onPressBack(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowBack}
                  />
                </TouchableOpacity>
              </View>

              <View style={{ position: 'fixed', bottom: '15%', right: '5%' }} >
                <TouchableOpacity onPress={this.onPressNext(match)}>
                  <Image
                    style={{ height: 60, width: 60 }}
                    source={ArrowNext}
                  />
                </TouchableOpacity>
              </View>
            </Header>
          </View>
        }
      </View>
    )
  }
}

const Travel = withRouter(Index)
export default Travel
