import React, { PureComponent } from 'react'
import { ImageBackground, TouchableHighlight, Linking, Image, View, Text } from 'react-native' 
import { withRouter } from 'react-router'
import ReactHoverObserver from 'react-hover-observer'
import GoogleMapReact from 'google-map-react'
import HoverImage from 'react-hover-image'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import Phone from '@fortawesome/fontawesome-free-solid/faPhone'
import Pin from '@fortawesome/fontawesome-free-solid/faMapPin'
import Mail from '@fortawesome/fontawesome-free-solid/faEnvelope'
import facebook from './images/facebook.png'
import instagram from './images/instagram.png'
import vk from './images/vk.png'
import searchLove from './images/searchLove.png'
import searchLoveHover from './images/searchLoveHover.png'
import enter from './images/enter.png'
import mapback from './images/mapback.png'
import enterHover from './images/enterHover.png'
import './marker.css'
import { constants, responsive, w, h } from '../../constants'

const { mobile5, mobile6, tablet, desktop, desktopHD } = responsive

const url1 = 'https://www.facebook.com/cityretreat.ru'
const url2 = 'https://www.instagram.com/cityretreat.ru/'
const url3 = 'https://vk.com/cityretreat'

const address = {
  yandexmap: 'https://yandex.ru/maps/213/moscow/?source=wizgeo&utm_source=serp&l=map&utm_medium=maps-desktop&mode=search&ll=37.555598%2C55.757850&text=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F%2C%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%2C%20%D0%A8%D0%BC%D0%B8%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BF%D1%80%D0%BE%D0%B5%D0%B7%D0%B4%2C%203%D1%811&sll=37.555598%2C55.758068&z=16',
  house: 'Москва, Шмитовский пр. 3с1',
  tel: '+7 (499)252-01-32',
  email: 'info@cityretreat.ru',
  day: 'Понедельник - пятница с 7:00 до 23:00',
  holyday: 'Суббота - воскресенье с 8:00 до 22:00'
}

const { yandexmap, house, tel, email, day, holyday } = address

const Map = () => (
  <GoogleMapReact
    bootstrapURLKeys={{ key: ['AIzaSyAxNWzDCwevNug1evmEGtbbVsEVgiSGb3g'] }}
    defaultCenter={{ lat: 55.757, lng: 37.5555 }}
    defaultZoom={16}
  >
    <AnyReactComponent
      lat={55.75805}
      lng={37.55595}
    />
  </GoogleMapReact>
)

const AnyReactComponent = () => (
  <View>
    <TouchableHighlight
      activeOpacity={0.2}
      underlayColor={constants.GOLDOPACITY} 
      onPress={() => Linking.openURL('https://yandex.ru/maps/213/moscow/?source=wizgeo&utm_source=serp&l=map&utm_medium=maps-desktop&mode=search&ll=37.555598%2C55.757850&text=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F%2C%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%2C%20%D0%A8%D0%BC%D0%B8%D1%82%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BF%D1%80%D0%BE%D0%B5%D0%B7%D0%B4%2C%203%D1%811&sll=37.555598%2C55.758068&z=16')}
    >
      <View className='pin' />
    </TouchableHighlight>
    <View className='pulse' />
  </View>
)

const Social = ({ isHovering = false, source, url }) => (
  <View>
    <TouchableHighlight
      activeOpacity={0.2}
      underlayColor={constants.GOLDOPACITY} 
      onPress={() => Linking.openURL(url)}
    >
      <Image source={source} style={{ height: 30, width: 30, margin: 2, opacity: isHovering ? 0.3 : 1 }} />
    </TouchableHighlight>
  </View>
)

const Call = ({ isHovering = false, source, url, text }) => (
  <TouchableHighlight
    activeOpacity={0.2}
    underlayColor={constants.GOLDOPACITY} 
    style={{ marginLeft: 0, justifyContent: 'flex-start' }}
    onPress={() => Linking.openURL(url)}
  >
    <View style={{ flex: 1, justifyContent: 'flex-start', paddingLeft: '6%', flexDirection: 'row' }}>
      <View style={{ transform: [{ rotate: isHovering ? '30deg' : '0deg' }] }}>
        <FontAwesomeIcon icon={source} style={{ height: 19, width: 19, color: '#000', opacity: isHovering ? 0.3 : 1 }} />
      </View>
      <Text style={[styles.h2Right, { paddingLeft: 6, alignSelf: 'center' }]}>{text}</Text>
    </View>
  </TouchableHighlight>
)

class Index extends PureComponent {
  handlePress(url) {
    Linking.canOpenURL(url).then(() => {
      return Linking.openURL(url)
    })
  }

  render() {
    const { container, page, line1, line2, box, box2, socialBox, h1, h2Left, hoverImg, contactBox, h2Right } = styles
    const { newheight } = this.props
    return (
      <View style={container}>
        { desktopHD === true &&
          <View style={[page, { marginTop: newheight }]}>
            <View style={line1}>
              <View style={box} >
                <HoverImage
                  src={enter}
                  hoverSrc={enterHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <HoverImage
                  src={searchLove}
                  hoverSrc={searchLoveHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <ImageBackground source={mapback} style={[hoverImg, { paddingBottom: 15, paddingRight: 13, paddingLeft: 3 }]}>
                  <Map />
                </ImageBackground>
              </View>
            </View>
            <View style={line2}>
              <View style={box2}>
                <Text style={h1}>Режим работы:</Text>
                <Text style={h2Left}>{day}</Text>
                <Text style={h2Left}>{holyday}</Text>
              </View>
              <View style={box2}>
                <View style={socialBox}>
                  <ReactHoverObserver>
                    <Social source={facebook} url={url1} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={instagram} url={url2} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={vk} url={url3} />
                  </ReactHoverObserver>
                </View>
              </View>
              <View style={box2}>
                <View style={contactBox}>
                  <ReactHoverObserver>
                    <Call source={Pin} text={house} url={yandexmap} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Phone} text={tel} url={'tel:+74992520132'} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Mail} text={email} url={'mailto:info@cityretreat.ru'} />
                  </ReactHoverObserver>
                </View>
              </View>
            </View>

            <Text style={[styles.h2Right, { marginTop: 50, paddingLeft: 6, alignSelf: 'center' }]}>© 2018, ООО "Сити Ретрит Клуб". Все права защищены.</Text>
            <TouchableHighlight activeOpacity={0.2} underlayColor={constants.GOLDOPACITY} onPress={() => this.props.history.push('/privacy')} >
              <Text style={[styles.h2Right, { paddingLeft: 6, marginBottom: 40, alignSelf: 'center' }]}>Политика конфиденциальности</Text>
            </TouchableHighlight>
          </View>
        }

        { desktop === true &&
          <View style={[page, { marginTop: newheight }]}>
            <View style={line1}>
              <View style={box} >
                <HoverImage
                  src={enter}
                  hoverSrc={enterHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <HoverImage
                  src={searchLove}
                  hoverSrc={searchLoveHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <ImageBackground source={mapback} style={[hoverImg, { paddingBottom: 15, paddingRight: 13, paddingLeft: 3 }]}>
                  <Map />
                </ImageBackground>
              </View>
            </View>
            <View style={line2}>
              <View style={box2}>
                <Text style={h1}>Режим работы:</Text>
                <Text style={h2Left}>{day}</Text>
                <Text style={h2Left}>{holyday}</Text>
              </View>
              <View style={box2}>
                <View style={socialBox}>
                  <ReactHoverObserver>
                    <Social source={facebook} url={url1} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={instagram} url={url2} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={vk} url={url3} />
                  </ReactHoverObserver>
                </View>
              </View>
              <View style={box2}>
                <View style={contactBox}>
                  <ReactHoverObserver>
                    <Call source={Pin} text={house} url={yandexmap} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Phone} text={tel} url={'tel:+74992520132'} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Mail} text={email} url={'mailto:info@cityretreat.ru'} />
                  </ReactHoverObserver>
                </View>
              </View>
            </View>

            <Text style={[styles.h2Right, { paddingLeft: 6, marginTop: 50, alignSelf: 'center' }]}>© 2018, ООО "Сити Ретрит Клуб". Все права защищены.</Text>
            <TouchableHighlight activeOpacity={0.2} underlayColor={constants.GOLDOPACITY} onPress={() => this.props.history.push('/privacy')} >
              <Text style={[styles.h2Right, { paddingLeft: 6, marginBottom: 40, alignSelf: 'center' }]}>Политика конфиденциальности</Text>
            </TouchableHighlight>
          </View>
        }

        { tablet === true &&
          <View style={[page, { marginTop: newheight }]}>
            <View style={line1}>
              <View style={box} >
                <HoverImage
                  src={enter}
                  hoverSrc={enterHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <HoverImage
                  src={searchLove}
                  hoverSrc={searchLoveHover}
                  style={hoverImg}
                />
              </View>
              <View style={box} >
                <ImageBackground source={mapback} style={[hoverImg, { paddingBottom: 15, paddingRight: 13, paddingLeft: 3 }]}>
                  <Map />
                </ImageBackground>
              </View>
            </View>
            <View style={line2}>
              <View style={box2}>
                <Text style={h1}>Режим работы:</Text>
                <Text style={h2Left}>{day}</Text>
                <Text style={h2Left}>{holyday}</Text>
              </View>
              <View style={box2}>
                <View style={socialBox}>
                  <ReactHoverObserver>
                    <Social source={facebook} url={url1} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={instagram} url={url2} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Social source={vk} url={url3} />
                  </ReactHoverObserver>
                </View>
              </View>
              <View style={box2}>
                <View style={contactBox}>
                  <ReactHoverObserver>
                    <Call source={Pin} text={house} url={yandexmap} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Phone} text={tel} url={'tel:+74992520132'} />
                  </ReactHoverObserver>
                  <ReactHoverObserver>
                    <Call source={Mail} text={email} url={'mailto:info@cityretreat.ru'} />
                  </ReactHoverObserver>
                </View>
              </View>
            </View>

            <Text style={[styles.h2Right, { marginTop: 50, paddingLeft: 6, alignSelf: 'center' }]}>© 2018, ООО "Сити Ретрит Клуб". Все права защищены.</Text>
            <TouchableHighlight activeOpacity={0.2} underlayColor={constants.GOLDOPACITY} onPress={() => this.props.history.push('/privacy')} >
              <Text style={[styles.h2Right, { paddingLeft: 6, marginBottom: 40, alignSelf: 'center' }]}>Политика конфиденциальности</Text>
            </TouchableHighlight>
          </View>
        }

        { mobile6 === true &&
          <View style={[page, { marginTop: 0 }]}>
            <View>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <HoverImage
                  src={enter}
                  hoverSrc={enterHover}
                  style={{ marginTop: 80, marginLeft: 3, justifyContent: 'center', height: w - 40, width: w - 40 }} 
                />
              </View>
              <View style={{ marginTop: 20 }}>
                <ReactHoverObserver>
                  <Call source={Pin} text={house} url={yandexmap} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Call source={Phone} text={tel} url={'tel:+74992520132'} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Call source={Mail} text={email} url={'mailto:info@cityretreat.ru'} />
                </ReactHoverObserver>
              </View>
              <View style={{ flex: 1, marginLeft: 20, justifyContent: 'flex-start', marginTop: 25 }} >
                <Text style={{ fontSize: 15, fontFamily: 'Museo500' }}>Режим работы:</Text>
                <Text style={h2Right}>{day}</Text>
                <Text style={h2Right}>{holyday}</Text>
              </View>
              <View style={socialBox}>
                <ReactHoverObserver>
                  <Social source={facebook} url={url1} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Social source={instagram} url={url2} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Social source={vk} url={url3} />
                </ReactHoverObserver>
              </View>
            </View>
            <View style={{ flex: 1, height: w - 60, width: w - 60, justifyContent: 'center', alignItems: 'center', margin: 15, shadowColor: '#000', shadowOffset: { width: 5, height: 11 }, shadowOpacity: 0.12, shadowRadius: 9 }}>
              <Map />
            </View>

            <Text style={[styles.h2Right, { paddingLeft: 6, alignSelf: 'center'}]}>© 2018, ООО "Сити Ретрит Клуб".</Text>
            <Text style={[styles.h2Right, { paddingLeft: 6, alignSelf: 'center'}]}>Все права защищены.</Text>
            <TouchableHighlight activeOpacity={0.2} underlayColor={constants.GOLDOPACITY} onPress={() => this.props.history.push('/privacy')} >
              <Text style={[styles.h2Right, { paddingLeft: 6, marginBottom: 40, alignSelf: 'center' }]}>Политика конфиденциальности</Text>
            </TouchableHighlight>
          </View>
        }

        { mobile5 === true &&
          <View style={[page, { marginTop: 0 }]}>
            <View style={{ flex: 1, justifyContent: 'flex-start' }}>
              <HoverImage
                src={enter}
                hoverSrc={enterHover}
                style={{ marginTop: 90, justifyContent: 'center', height: w - 10, width: w - 10 }}
              />
              <ReactHoverObserver>
                <Call source={Pin} text={house} url={yandexmap} />
              </ReactHoverObserver>
              <ReactHoverObserver>
                <Call source={Phone} text={tel} url={'tel:+74992520132'} />
              </ReactHoverObserver>
              <ReactHoverObserver>
                <Call source={Mail} text={email} url={'mailto:info@cityretreat.ru'} />
              </ReactHoverObserver>
              <View style={{ flex: 1, marginLeft: 15, marginTop: 25 }} >
                <Text style={{ fontSize: 15, fontFamily: 'Museo500' }}>Режим работы:</Text>
                <Text style={h2Right}>{day}</Text>
                <Text style={h2Right}>{holyday}</Text>
              </View>
              <View style={socialBox}>
                <ReactHoverObserver>
                  <Social source={facebook} url={url1} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Social source={instagram} url={url2} />
                </ReactHoverObserver>
                <ReactHoverObserver>
                  <Social source={vk} url={url3} />
                </ReactHoverObserver>
              </View>
            </View>
            <View >
              <ImageBackground source={mapback} style={[{ height: 300, width: 300, marginTop: 30, paddingBottom: 15 }]}>
                <Map />
              </ImageBackground>
            </View>

            <Text style={[styles.h2Right, { paddingLeft: 6, alignSelf: 'center'}]}>© 2018, ООО "Сити Ретрит Клуб".</Text>
            <Text style={[styles.h2Right, { paddingLeft: 6, alignSelf: 'center'}]}>Все права защищены.</Text>
            <TouchableHighlight activeOpacity={0.2} underlayColor={constants.GOLDOPACITY} onPress={() => this.props.history.push('/privacy')} >
              <Text style={[styles.h2Right, { paddingLeft: 6, marginBottom: 40, alignSelf: 'center' }]}>Политика конфиденциальности</Text>
            </TouchableHighlight>
          </View>
        }
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  line1: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: mobile5 ? 150 : mobile6 ? 150 : tablet ? 150 : desktop ? 150 : desktopHD ? 250 : 150
  },
  line2: {
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  box: {
    flex: 1,
    height: h / 2.3,
    width: h / 2.3,
    margin: 20
  },
  box2: {
    flex: 1,
    width: h / 2.3,
    paddingRight: 10,
    margin: 15
  },
  socialBox: {
    flex: 1, 
    flexDirection: 'row', 
    paddingTop: mobile5 ? 30 : 20,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  contactBox: {
    flex: 1, 
    justifyContent: 'flex-start'
  },
  h1: {
    fontSize: 23,
    fontFamily: 'Museo500',
    alignSelf: 'flex-end'
  },
  h2Left: {
    fontSize: mobile5 ? 12 : mobile6 ? 15 : tablet ? 12 : desktop ? 16 : desktopHD ? 19 : 15, 
    fontFamily: 'CirceExtraLight',
    color: '#000',
    alignSelf: 'flex-end'
  },
  h2Right: {
    fontSize: mobile5 ? 12 : mobile6 ? 15 : tablet ? 12 : desktop ? 16 : desktopHD ? 19 : 15, 
    fontFamily: 'CirceExtraLight',
    color: '#000',
    alignSelf: 'flex-start'
  },
  social: {
    height: 30,
    width: 30,
    margin: 1
  },
  hoverImg: {
    height: h / 2.3,
    width: h / 2.3
  }
}

const Contact = withRouter(Index)
export default Contact
