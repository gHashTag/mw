import React from 'react'
import { Dimensions, View, Text, StyleSheet } from 'react-native'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class TextHistory extends React.PureComponent {
  render() {
    const { StartDate, FullName, Room } = this.props
    const { containerStyle, labelStyle, fullNameStyle, dateStyle, inputStyle } = styles
    return (
      <View style={containerStyle}>
        <View style={dateStyle}>
          <Text style={labelStyle}>{StartDate.substr(5)}</Text>
        </View>
        <View style={fullNameStyle}>
          <Text numberOfLines={1} ellipsizeMode='tail' style={inputStyle}>{FullName}</Text>
        </View>
        <Text numberOfLines={1} ellipsizeMode='tail' style={inputStyle}>{Room}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    height: 40,
    width: mobile5 ? 300 : mobile6 ? 340 : tablet ? 600 : desktop ? 600 : '',
    flexDirection: 'row',
    alignItems: 'center'
  },
  labelStyle: {
    flex: 1,
    fontFamily: constants.CirceLight,
    color: constants.SECONDARY,
    fontSize: mobile5 ? 14 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '',
    paddingLeft: 10
  },
  dateStyle: {
    width: mobile5 ? 50 : mobile6 ? 60 : tablet ? 180 : desktop ? 180 : ''
  },
  fullNameStyle: {
    width: mobile5 ? 140 : mobile6 ? 200 : tablet ? 300 : desktop ? 300 : ''
  },
  inputStyle: {
    flex: 1,
    color: '#000',
    textAlign: 'left',
    paddingLeft: 8,
    fontSize: mobile5 ? 14 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '',
    lineHeight: 23,
    fontFamily: constants.CirceLight
  }
})

export { TextHistory }
