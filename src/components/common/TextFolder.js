import React from 'react'
import { Dimensions, View, Text, StyleSheet } from 'react-native'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class TextFolder extends React.PureComponent {
  render() {
    const { value, label } = this.props
    const { containerStyle, labelStyle, inputStyle } = styles
    return (
      <View style={containerStyle}>
        <View style={{ width: 140 }}>
          <Text style={labelStyle}>{label}</Text>
        </View>
        <Text style={inputStyle}>{value}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    height: 40,
    width: mobile5 ? 300 : mobile6 ? 340 : tablet ? 600 : desktop ? 600 : '', //eslint-disable-line
    flexDirection: 'row',
    alignItems: 'center'
  },
  labelStyle: {
    flex: 1,
    fontFamily: constants.CirceLight,
    color: constants.SECONDARY,
    fontSize: mobile5 ? 14 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '', //eslint-disable-line
    paddingLeft: 10
  },
  inputStyle: {
    flex: 1,
    color: '#000',
    textAlign: 'left',
    paddingLeft: 8,
    fontSize: mobile5 ? 14 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '', //eslint-disable-line
    lineHeight: 23,
    fontFamily: constants.CirceLight
  }
})

export { TextFolder }
