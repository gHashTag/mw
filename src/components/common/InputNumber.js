import React from 'react'
import { TextInput, View, Text, StyleSheet } from 'react-native'
import { constants } from '../../constants'

class InputNumber extends React.PureComponent {
  onChangeText = (text) => {
    const { onChangeText, name } = this.props
    const newText = text.replace(/[^0-9]/g, '')
    onChangeText(name, newText)
  }

  render() {
    const { value, label, editable, secureTextEntry, placeholder } = this.props
    const { containerStyle, labelStyle, inputStyle } = styles
    const newValue = value.replace(/[^0-9]/g, '')
    return (
      <View style={containerStyle}>
        <Text style={labelStyle}>{label}</Text>
        <TextInput
          onChangeText={this.onChangeText}
          value={newValue}
          style={inputStyle}
          placeholder={placeholder}
          editable={editable}
          secureTextEntry={!!secureTextEntry}
          placeholderTextColor={constants.LIGHT_GRAY}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 8,
    fontSize: 18,
    lineHeight: 23,
    fontFamily: 'CirceLight',
    flex: 2
  },
  labelStyle: {
    fontFamily: 'CirceLight',
    fontSize: 18,
    paddingLeft: 10,
    flex: 1
  },
  containerStyle: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
})

export { InputNumber }
