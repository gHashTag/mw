import React from 'react'
import { StyleSheet, View } from 'react-native'
import Spinner from 'react-spinkit'
import { constants } from '../../constants'

const Loading = () => {
  return (
    <View style={styles.container}>
      <Spinner 
        name='ball-scale-ripple-multiple' 
        color={constants.SECONDARY} 
      /> 
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: '47%',
    left: '49%'
  }
})

export { Loading }
