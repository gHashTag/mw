import React, { PureComponent } from 'react'
import { Dimensions, View, Image, TouchableHighlight, StyleSheet } from 'react-native' 
import { withRouter } from 'react-router'
import { constants } from '../../constants'
import key from './key.svg'

const win = Dimensions.get('window')
const w = win.width

class Index extends PureComponent {
  goToProfile = () => {
    this.props.history.push('/checktoken')
  }

  render() {
    const { container, profile } = styles 
    return (
      <View style={container}>
        <TouchableHighlight
          onPress={this.goToProfile}
          activeOpacity={0.3}
          underlayColor={constants.GOLDOPACITY} 
          style={{ width: 300 }}
        >
          <Image style={profile} source={key} />
        </TouchableHighlight>
      </View>
    )
  }
}

const platform = (x) => {
  try {
    if (w <= 320) {
      return x - 5 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x + 5 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x + 9 // 'iphone6+'
    } else if (w > 414) {
      return x * 3.9 // 'web'
    }
  } catch (e) {
    throw e
  }
}

const menuY = (x) => {
  try {
    if (w <= 320) {
      return x + 8 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x + 8 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x + 8 //'iphone6+'
    } else if (w > 414) {
      return x + 23// 'web'
    }
  } catch (e) {
    throw e
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    zIndex: 13
  },
  profile: {
    position: 'fixed', 
    width: 50,
    height: 50,
    zIndex: 14,
    right: platform(5) + 9,
    top: menuY(1) 
  }
})

const User = withRouter(Index)
export default User
