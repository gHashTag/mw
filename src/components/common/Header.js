import React, { PureComponent } from 'react'
import { Image, Dimensions, TouchableHighlight, Text, StyleSheet, View } from 'react-native' 
import { withRouter } from 'react-router'
import Mansion from './Mansion.svg'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Index extends PureComponent {
  goToHome = () => {
    window.location.href = constants.SITE 
  }

  render() {
    const { title } = this.props
    const { container, h1 } = styles 
    return (
      <View>
        { desktop === true &&
          <View>
            <View style={container}>
              <TouchableHighlight
                onPress={this.goToHome}
                activeOpacity={0.3}
                underlayColor={constants.OPACITY} 
                style={{ width: 320 }}
              >
                <Image source={Mansion} style={{ width: 328, height: 70 }} />
              </TouchableHighlight>
            </View>
            {this.props.children}
          </View>
        }

        { tablet === true &&
          <View>
            <View style={container}>
              <TouchableHighlight
                onPress={this.goToHome}
                activeOpacity={0.3}
                underlayColor={constants.OPACITY} 
                style={{ width: 320 }}
              >
                <Image source={Mansion} style={{ width: 328, height: 70 }} />
              </TouchableHighlight>
            </View>
            {this.props.children}
          </View>
        }

        { mobile6 === true &&
          <View>
            <View style={[container, { height: 60 }]}>
              <Text style={[h1, { paddingTop: 0, fontSize: 21 }]}>
                {title}
              </Text>
            </View>
            {this.props.children}
          </View>
        }

        { mobile5 === true &&
          <View>
            <View style={[container, { height: 60 }]}>
              <Text style={[h1, { paddingTop: 0, fontSize: 18 }]}>
                {title}
              </Text>
            </View>
            {this.props.children}
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 90,
    width: '100%',
    borderBottomWidth: 0,
    shadowColor: '#9B9B9B',
    shadowOffset: { width: 0, height: 2.5 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    backgroundColor: '#fff',
    flexDirection: 'row',
    position: 'fixed',
    zIndex: 2
  },
  h1: {
    flex: 1,
    color: '#4B4B4B',
    fontFamily: 'CirceLight',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: '100',
    textAlign: 'center'
  }
})

const Header = withRouter(Index)
export { Header }
