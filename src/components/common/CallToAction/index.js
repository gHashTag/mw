import React, { PureComponent } from 'react'
import { View, TouchableHighlight, CheckBox, Text } from 'react-native'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Recaptcha from 'react-recaptcha'
import { Card, Done, CardSection, Input, Spinner, InputNumber, ButtonSignIn } from '../'
import { setGroupAppointmentNew, spinner, clearStore } from '../../../actions'
import { constants } from '../../../constants'

//const win = Dimensions.get('window')
//const w = win.width
//const mobile5 = w > 315 && w < 341
//const mobile6 = w > 342 && w < 415
//const tablet = w < 991 && w > 415
//const desktop = w > 992

const defaultState = {
  values: {
    name: '',
    email: ''
  },
  errors: {},
  trueCheckBoxIsOn: true,
  captcha: false,
  check: false
}

class CallToAction extends PureComponent {
  state = defaultState

  componentDidMount() {
    this.props.clearStore()
  }

  onChangeText = (key, value) => {
    this.setState(state => ({
      values: {
        ...state.values,
        [key]: value
      }
    }))
  }

  submit = async () => {
    try {
      this.props.spinner(true)
      const { title, subtitle, StartDate } = this.props
      const { values: { name, email } } = this.state
      const ClientID = `${name} - ${email}` 
      const Employee = { FullName: title }
      const Service = { Title: subtitle }
      this.props.setGroupAppointmentNew({ ClientID, Employee, Service, StartDate })
    } catch (err) {
      this.setState({
        errors: {
          name: 'Что-то пошло не так. Попробуйте еще раз.'
        },
        isSubmitting: true 
      })
      return
    }
    this.setState(defaultState)
  }

  goToPrivacy = () => {
    window.location.href = 'https://www.cityretreat.ru/privacy' 
  }

  verifyCallback = (response) => {
    response ? this.setState({ captcha: true }) : this.setState({ captcha: false })
  }

  done = () => {
    const { trueCheckBoxIsOn, captcha, values: { name, email } } = this.state
    if (trueCheckBoxIsOn === true && captcha === true && name !== '' && email !== '') { return true }
  }

  _renderCallToAction = () => {
    const { Reserved } = this.props.data
    const { values: { name, email }, trueCheckBoxIsOn } = this.state
    if (Reserved) {
      return (
        <View style={{ flex: 1, paddingBottom: 100, alignItems: 'center' }}>
          <Done />
        </View>
      )
    } else {
      return (
        <Card>
          <CardSection>
            <InputNumber
              value={name}
              label='Phone +7'
              placeholder='9998887766'
              name='name'
              onChangeText={this.onChangeText}
            />
          </CardSection>
          <CardSection>
            <Input
              value={email}
              label='Имя'
              placeholder='Владимир'
              name='email'
              autoCapitalize='none'
              onChangeText={this.onChangeText}
            />
          </CardSection>

          <View style={{ paddingTop: 10, paddingBottom: 0, alignSelf: 'center' }}>
            <Recaptcha
              sitekey="6LdP7F8UAAAAAOWlyVPW5BN8DMLmySk9W_wc5fl0"
              render="explicit"
              verifyCallback={this.verifyCallback}
            />
          </View>

          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', paddingTop: 15, paddingBottom: 5, alignSelf: 'center'}}>
            <CheckBox
              onValueChange={value => this.setState({trueCheckBoxIsOn: value})}
              color={constants.SECONDARY}
              value={trueCheckBoxIsOn}
            />
            <TouchableHighlight
              onPress={this.goToPrivacy}
              activeOpacity={0.3}
              underlayColor={constants.GOLDOPACITY} 
            >
              <Text style={{ fontFamily: constants.FONTLIGHT, color: constants.PRIMARY, paddingLeft: 10 }}>Принимаю соглашение сайта</Text>
            </TouchableHighlight>
          </View>

          <View style={{ paddingTop: 15, paddingBottom: 15, alignSelf: 'center' }}>
            <ButtonSignIn 
              disabled={!this.done()}
              title='Записаться' 
              onPress={this.submit} 
            />
          </View>
        </Card>
      )
    }
  }

  render() {
    const { errors } = this.state
    const { loading } = this.props.data
    return (
      <View> 
        <View style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: 200 }}>
          {errors.name && 
            <View style={{ flex: 1, width: 300, alignSelf: 'center' }}>
              <Text style={{ padding: 15, fontFamily: constants.FONTLIGHT, color: 'red' }}>{errors.name}</Text>
            </View>
          }
          { loading && 
            <Spinner />
          }

          { !loading && 
            <View>
              {this._renderCallToAction()}
            </View>
          }

        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return { 
    data: state.mail
  }
}

export default withRouter(connect(mapStateToProps, { setGroupAppointmentNew, spinner, clearStore })(CallToAction))
