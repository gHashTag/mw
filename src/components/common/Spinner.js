import React from 'react'
import { View, StyleSheet } from 'react-native'
import SpinnerKit from 'react-spinkit'
import { constants } from '../../constants'

const Spinner = () => {
  return (
    <View style={styles.container}>
      <SpinnerKit name='ball-scale-ripple-multiple' color={constants.SECONDARY} /> 
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    paddingTop: 15, 
    paddingBottom: 15 
  }
})

export { Spinner }
