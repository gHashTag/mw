import React, { PureComponent } from 'react'
import { Dimensions } from 'react-native'
import arrow from './arrow.svg'
import './index.css'
import '../../aos.css'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktopHD = w >= 1920

class MouseScroller extends PureComponent {
  render() {
    return (
      <img 
        style={{ position: 'fixed', left: mobile5 ? '42%' : mobile6 ? '44%' : tablet ? '47%' : desktopHD ? '48.7%' : '48.0%' }} 
        className="down-scroll" 
        src={arrow} 
        alt="" 
        width="50" 
      />
    )
  }
}

export default MouseScroller
