import React, { PureComponent } from 'react'
import { View, Image, Dimensions } from 'react-native'

const win = Dimensions.get('window')
const h = win.height
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415

export default class BoxAnimateLeft extends PureComponent {
  render() {
    const { open, tx, ty, source, distance } = this.props
    const newH = h - (mobile5 ? 60 : mobile6 ? 60 : 90)
    return (
      <View style={{
        position: 'fixed',
        zIndex: open ? 0 : 3,
        right: distance 
      }}
      >
        <Image 
          style={{
            marginTop: mobile5 ? 60 : mobile6 ? 60 : 90,
            paddingLeft: -w / 2, 
            height: newH,
            width: newH / 2.68,
            transform: [
              { translateX: tx },
              { translateY: ty }
            ]
          }} 
          source={source}
        />
      </View>
    )
  }
}
