import React, { PureComponent } from 'react'
import { Dimensions, View } from 'react-native'
import BoxAnimateLeft from './BoxAnimateLeft'
import BoxAnimateRight from './BoxAnimateRight'

const win = Dimensions.get('window')
const h = win.height
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 414
const mobile6plus = w >= 414 && w < 420
const tablet = w <= 1024 && w > 420
const desktop = w < 1920 && w > 1024 
const desktopHD = w >= 1920

class Door extends PureComponent {
  state = {
    scroll: window.scrollY,
    open: false
  }

  componentDidMount() {
    //console.log('navigator.userAgent', navigator.userAgent)
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    this.setState({ scroll: window.scrollY })
    const { scroll } = this.state
    scroll > (mobile5 ? 150 : mobile6 ? 185 : mobile6plus ? 200 : tablet ? h : desktop ? h : desktopHD ? h : 100) ? this.setState({ open: true }) : this.setState({ open: false })
  }

  render() {
    const { scroll, open } = this.state
    return (
      <View style={{ height: this.props.height }}>
        <BoxAnimateLeft open={open} tx={-scroll / 3} distance={'85%'} ty={0} source={require('./image/Door.png')} />
        <BoxAnimateLeft open={open} tx={-scroll / 1.5} distance={'67.5%'} ty={0} source={require('./image/Door.png')} />
        <BoxAnimateLeft open={open} scroll={scroll} tx={-scroll} distance={'50%'} ty={0} source={require('./image/Door.png')} />
        <BoxAnimateRight open={open} tx={scroll / 3} distance={'85%'} ty={0} source={require('./image/Door.png')} />
        <BoxAnimateRight open={open} tx={scroll / 1.5} distance={'67.5%'} ty={0} source={require('./image/Door.png')} />
        <BoxAnimateRight open={open} tx={scroll} distance={'50%'} ty={0} source={require('./image/Door.png')} />
        {this.props.children}
      </View>
    )
  }
}

export default Door 
