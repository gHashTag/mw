import React from 'react'
import { View, Dimensions, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const ButtonUI = ({ isHovering = false, title, onPress, disabled}) => {
  return (
    <TouchableWithoutFeedback 
      disabled={disabled}
      onPress={onPress}
    >
      <View style={[styles.buttonStyle, { backgroundColor: isHovering ? constants.SECONDARY : 'transparent'}]}>
        <Text style={[styles.buttonTextStyle, { color: isHovering ? constants.WHITE : constants.SECONDARY }]}>{title}</Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  buttonStyle: {
    height: mobile5 ? 28 : mobile6 ? 30 : tablet ? 35 : desktop ? 40 : '', 
    width: mobile5 ? 100 : mobile6 ? 100 : tablet ? 130 : desktop ? 135 : '', 
    borderColor: constants.SECONDARY,
    borderWidth: 2,
    justifyContent: 'center'
  },
  buttonTextStyle: {
    fontSize: mobile5 ? 12 : mobile6 ? 14 : tablet ? 17 : desktop ? 17 : '',
    fontFamily: 'CirceLight',
    paddingTop: 1,
    fontWeight: '400',
    textAlign: 'center',
    color: constants.SECONDARY 
  }
})

export { ButtonUI }
