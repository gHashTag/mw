import React, { Component } from 'react'
import { View, Dimensions, Text } from 'react-native'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faExclamationCircle from '@fortawesome/fontawesome-free-solid/faExclamationCircle'
import { constants } from '../../constants'
import { Header } from './'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Error extends Component {
  render() {
    const { container, icons, h1, h2 } = styles
    return (
      <View>
        <Header title='Ошибка' />
        <View style={container}>
          <FontAwesomeIcon style={icons} icon={faExclamationCircle} />
          <Text style={h1}>Ошибка</Text>
          <Text style={h2}>Что-то не так. Попробуйте еще раз.</Text>
        </View>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icons: {
    position: 'fixed',
    top: mobile5 ? '30%' : mobile6 ? '32%' : tablet ? '36%' : desktop ? '33%' : '', //eslint-disable-line
    width: 100,
    height: 100,
    color: 'red' 
  },
  h1: {
    position: 'fixed',
    top: '50%',
    fontFamily: 'Museo500', 
    fontSize: mobile5 ? 16 : mobile6 ? 23 : tablet ? 35 : desktop ? 35 : '', //eslint-disable-line
  },
  h2: {
    position: 'fixed',
    top: mobile5 ? '55%' : mobile6 ? '55%' : tablet ? '56%' : desktop ? '58%' : '', //eslint-disable-line
    fontFamily: constants.FONTLIGHT, 
    fontSize: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 16 : '', //eslint-disable-line
  }
}

export default Error 
