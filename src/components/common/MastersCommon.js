import React, { PureComponent } from 'react'
import { View, Image, Dimensions, StyleSheet, TouchableHighlight } from 'react-native'
import { withRouter } from 'react-router'
import masters from '../../actions/masters/masters.png'
import frame from '../../actions/masters/frame.png'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const url = '/master/feliks-pak'

class Index extends PureComponent {
  handlePress() {
    this.props.history.push(url)
  }

  render() {
    const { container, mastersstyle, framestyle } = styles
    return (
      <View>
        <View style={container}>
          <TouchableHighlight
            activeOpacity={0.2}
            onPress={() => this.handlePress()}
          >
            <Image source={frame} style={framestyle} />
          </TouchableHighlight>
        </View>
        <Image source={masters} style={mastersstyle} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center', 
    paddingBottom: mobile5 ? 50 : mobile6 ? 50 : tablet ? 100 : desktop ? 100 : '', //eslint-disable-line
  },
  mastersstyle: {
    width: mobile5 ? win.width : mobile6 ? win.width : tablet ? win.width - 40 : desktop ? win.width : '', //eslint-disable-line
    height: mobile5 ? 200 : mobile6 ? 200 : tablet ? win.width * 0.2 : desktop ? win.width * 0.2 : '', //eslint-disable-line
  },
  framestyle: {
    top: mobile5 ? 40 : mobile6 ? 50 : tablet ? 60 : desktop ? 60 : '', //eslint-disable-line
    width: mobile5 ? 240 : mobile6 ? 260 : tablet ? 260 : desktop ? 280 : '', //eslint-disable-line
    height: mobile5 ? 200 : mobile6 ? 217 : tablet ? 217 : desktop ? 234 : '', //eslint-disable-line
  }
})

const MastersCommon = withRouter(Index)
export default MastersCommon 
