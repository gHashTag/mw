import React, { Component } from 'react'
import { View, Dimensions, Text } from 'react-native'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faCheckCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Done extends Component {
  render() {
    const { container, icons, h1, h2 } = styles
    return (
      <View style={container}>
        <FontAwesomeIcon style={icons} icon={faCheckCircle} />
        <Text style={h1}>Успешно</Text>
        <Text style={h2}>Скоро с вами свяжется администратор.</Text>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  icons: {
    width: mobile5 ? 60 : mobile6 ? 70 : 100,
    height: mobile5 ? 60 : mobile6 ? 70 : 100, 
    color: constants.SECONDARY
  },
  h1: {
    fontFamily: 'Museo500', 
    fontSize: mobile5 ? 16 : mobile6 ? 23 : tablet ? 35 : desktop ? 35 : '' 
  },
  h2: {
    fontFamily: constants.FONTLIGHT, 
    fontSize: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 16 : '' 
  }
}

export { Done }
