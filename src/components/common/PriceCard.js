import React, { PureComponent } from 'react'
import { Dimensions, View, Text } from 'react-native' 
//import ReactHoverObserver from 'react-hover-observer'
import { withRouter } from 'react-router'
//import { ButtonUI } from '../common'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w < 1920 && w >= 991 

class Index extends PureComponent {
  goToPay = () => {
    const { price, title, status } = this.props.cardinfo
    const order_desc = `${title} ${status}`
    const amount = price * 100
    const obj = ({ order_desc, amount })
    this.props.history.push('./checktopay', obj) 
  }

  render() {
    const { h1, h2, h3 } = styles
    const { title, status, price, info1, info2, info3, info4 } = this.props.cardinfo
    const { backgroundColor, borderColor, h1color, h2color, h3color } = this.props.colorcard
    const cardH = 396
    const cardW = 250 
    return (
      <View 
        style={{ 
          backgroundColor,
          margin: 1,
          borderColor,
          borderWidth: 2,
          borderRadius: 20,
          height: desktop ? cardH * 0.80 : tablet ? cardH * 0.60 : cardH * 0.95,
          width: desktop ? cardW * 0.80 : tablet ? cardW * 0.60 : cardW * 0.95,
          shadowColor: '#9B9B9B',
          shadowOffset: { width: 2, height: 2 },
          shadowOpacity: 0.1,
          shadowRadius: 8
        }}
      >
        <Text style={[h2, { marginTop: tablet ? 20 : 40, color: h2color }]}>{title}</Text>
        <Text style={[h2, { color: h2color }]}>{status}</Text>
        <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10 }}>
          <Text style={{ color: h1color, fontSize: tablet ? 25 : 35, paddingTop: 0.5, fontFamily: 'Roboto' }}>{'\u20bd'} </Text>
          <Text 
            style={[h1, { color: h1color }]}
          >
            {price}
          </Text>
        </View>
        <Text style={[h3, { marginTop: 10, color: h3color }]}>{info1}</Text>
        <Text style={[h3, { color: h3color }]}>{info2}</Text>
        <Text style={[h3, { color: h3color }]}>{info3}</Text>
        <Text style={[h3, { color: h3color }]}>{info4}</Text>
      </View>
    )
  }
}

        //<View style={{ position: 'absolute', bottom: 30, alignSelf: 'center' }}>
          //<ReactHoverObserver>
            //<ButtonUI title='Купить' onPress={this.goToPay} />
          //</ReactHoverObserver>
        //</View>
const styles = {
  h1: {
    fontFamily: 'Museo500', 
    fontSize: tablet ? 25 : 35  
  },
  h2: {
    fontFamily: 'Museo500', 
    fontSize: tablet ? 14 : 20, 
    marginTop: 0, 
    alignSelf: 'center' 
  },
  h3: {
    fontFamily: 'CirceLight', 
    fontSize: mobile5 ? 14 : mobile6 ? 15 : tablet ? 10 : 13,
    marginTop: 0, 
    width: tablet ? 150 : 190, 
    textAlign: 'center', 
    alignSelf: 'center' 
  }
}

const PriceCard = withRouter(Index)

export default PriceCard
