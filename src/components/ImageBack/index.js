import React, { PureComponent } from 'react'
import { StyleSheet, Dimensions, View, Image } from 'react-native' // eslint-disable-line
import back from './back.png'

const win = Dimensions.get('window')
const w = win.width
const h = win.height
const mobile5 = w > 315 && w <= 341
const mobile6 = w > 341 && w < 376

class ImageBack extends PureComponent {
  render() {
    const { backgroundImage } = styles
    return (
      <View>
        <Image
          style={backgroundImage}
          source={back}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'fixed', 
    left: 0,
    zIndex: -1, 
    width: mobile5 ? 320 : mobile6 ? 375 : 414, 
    height: h
  }
})

export default ImageBack 
