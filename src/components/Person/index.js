import React from 'react'
import { Helmet } from 'react-helmet'
import Person from './Person'
import { Header } from '../common'

export default () => (
  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Личный кабинет·Mansion Wellness</title>
      <link rel="canonical" href="http://cityretreat.ru/contacts" />
      <meta name="keywords" content="йога, сити ретрит, spa, wellness, спа, велнес" />
      <meta property="og:image" content='http://www.cityretreat.ru/static/media/logo.162e94fa.jpg' />
      <meta name="description" content="Особняк в центре Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы. Уникальный формат Центра – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей." />
    </Helmet>

    <Header title='Личный кабинет'>
      <Person />
    </Header>
  </div>
) 
