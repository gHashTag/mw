import React, { PureComponent } from 'react'
import { Dimensions, View } from 'react-native'
import { connect } from 'react-redux'
import 'react-tabs/style/react-tabs.css'
import { getVisitHistory } from '../../actions'
import { TextHistory, Card, CardSection } from '../common'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class History extends PureComponent {
  async componentDidMount() {
    const { ClientID } = this.props.clientData
    this.props.getVisitHistory(ClientID)
  }

  render() {
    const { container, subContainer } = styles
    const { historyData } = this.props
    return (
      <View style={{ paddingTop: 20, paddingBottom: 100 }}>
        <Card style={container}>
          { 
            historyData.map(({ Id, AppointmentList }) => {
              return (
                <View key={Id} style={subContainer} >
                  { 
                    AppointmentList.map(({ AppointmentID, StartDate, Employee, Room }) => {
                      return (
                        <CardSection key={AppointmentID} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                          <TextHistory
                            StartDate={StartDate}
                            FullName={Employee.FullName}
                            Room={Room.Title}
                          />
                        </CardSection>
                      )
                    })
                  }
                </View>
              )
            })
          }
        </Card>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  subContainer: {
    width: mobile5 ? 300 : mobile6 ? 340 : tablet ? 600 : desktop ? 600 : '', //eslint-disable-line
  }
}

const mapStateToProps = state => {
  return { 
    clientData: state.me.clientData,
    historyData: state.me.historyData
  }
}

export default connect(mapStateToProps, { getVisitHistory })(History)
