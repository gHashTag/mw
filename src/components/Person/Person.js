import React, { PureComponent } from 'react'
import { Dimensions, View } from 'react-native'
import { withRouter } from 'react-router'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import Profile from './Profile'
import History from './History'
import { constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Index extends PureComponent {
  render() {
    const { container, tab, tabStyle } = styles
    return (
      <View style={container}>
        <Tabs>
          <View style={tabStyle}>
            <TabList>
              <Tab style={tab}>Персональные данные</Tab>
              <Tab style={tab}>История посещений</Tab>
            </TabList>
          </View>

          <TabPanel>
            <Profile />
          </TabPanel>

          <TabPanel>
            <History />
          </TabPanel>
        </Tabs>

      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    paddingTop: mobile5 ? 50 : mobile6 ? 50 : tablet ? 130 : desktop ? 130 : '', //eslint-disable-line
    justifyContent: 'center',
    alignItems: 'center'
  },
  tab: {
    fontFamily: constants.CirceLight,
    fontSize: mobile5 ? 13 : mobile6 ? 15 : tablet ? 18 : desktop ? 18 : '', //eslint-disable-line
    marginTop: 50
  },
  tabStyle: {
    paddingLeft: 7, 
    paddingRight: 7
  }
}

const Person = withRouter(Index)

export default Person

