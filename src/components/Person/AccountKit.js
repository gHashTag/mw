import React from 'react'

class AccountKit extends React.Component {
  constructor(props) {
    super(props)
    this.signIn = this.signIn.bind(this)
    this.state = {
      disabled: false
    }
  }

  componentDidMount() {
    if (!window.AccountKit) {
      (cb => {
        const tag = document.createElement('script')
        tag.setAttribute(
          'src',
          'https://sdk.accountkit.com/ru_RU/sdk.js'
        )
        tag.setAttribute('id', 'account-kit')
        tag.setAttribute('type', 'text/javascript')
        tag.onload = cb
        document.head.appendChild(tag)
      })(() => {
        window.AccountKit_OnInteractive = this.onLoad.bind(this)
      })
    }
  }

  onLoad() {
    const { appId, csrf, version } = this.props
    window.AccountKit.init({
      appId,
      debug: true,
      state: csrf,
      version,
      fbAppEventsEnabled: true 
    })
    this.setState({
      disabled: false
    })
  }

  signIn() {
    if (this.state.disabled) {
      return
    }
    const {
      loginType,
      onResponse,
      countryCode,
      phoneNumber,
      emailAddress
    } = this.props

    const options = {}
    if (countryCode) {
      options.countryCode = countryCode
    }

    if (loginType === 'PHONE' && phoneNumber) {
      options.phoneNumber = phoneNumber
    } else if (loginType === 'EMAIL' && emailAddress) {
      options.emailAddress = emailAddress
    }

    window.AccountKit.login(loginType, options, resp => onResponse(resp))
  }

  render() {
    const disabled = this.state.disabled || this.props.disabled
    return this.props.children({
      onPress: () => {
        this.signIn()
      },
      disabled
    })
  }
}

AccountKit.defaultProps = {
  disabled: false,
  language: 'ru_RU',
  loginType: 'PHONE'
}

export default AccountKit
