import React, { Component } from 'react'
import { AsyncStorage, StyleSheet, Text, Dimensions, View } from 'react-native'
import ReactHoverObserver from 'react-hover-observer'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { getScheduleItem, setGroupAppointment, setGroupAppointmentNew, cancelGroupAppointmentNew, cancelGroupAppointment, spinner } from '../../actions'
import { Header, ButtonUI, TextFolder, Card, CardSection, Spinner } from '../common'
import { TOKEN_KEY, PHONE, constants } from '../../constants'

const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

class Index extends Component {
  state = {
    data: {},
    newclient: false
  }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem(TOKEN_KEY)
    if (!token) {
      this.props.history.push('/checktoken')
      return
    }
    try {
      const { ClientID } = this.props.clientData
      const { AppointmentID } = this.props.location.state
      this.props.getScheduleItem({ ClientID, AppointmentID })
    } catch (e) {
      console.log('Reserve error', e)
    }
  }

  _signup = () => {
    this.props.spinner(false)
    const { AppointmentID } = this.props.location.state
    const { ClientID } = this.props.clientData
    console.log('ClientID', ClientID)
    this.props.setGroupAppointment({ ClientID, AppointmentID })
  }

  _cancel = () => {
    this.props.spinner(false)
    const { AppointmentID } = this.props.location.state
    const { ClientID } = this.props.clientData
    console.log('ClientID', ClientID)
    this.props.cancelGroupAppointment({ ClientID, AppointmentID })
  }

  _renderButton = () => {
    const { Reserved, loading } = this.props.data
    if (!loading) { return <Spinner /> }
    if (Reserved) {
      return (
        <ReactHoverObserver>
          <ButtonUI title="Отмена" onPress={this._cancel} />
        </ReactHoverObserver>
      )
    } else {
      return (
        <ReactHoverObserver>
          <ButtonUI title="Запись" onPress={this._signup} />
        </ReactHoverObserver>
      )
    }
  }

  _signupNew = async () => {
    this.props.spinner(true)
    const ClientID = await AsyncStorage.getItem(PHONE)
    const { Employee, Service, StartDate } = this.props.data.item
    this.props.setGroupAppointmentNew({ ClientID, Employee, Service, StartDate })
  }

  _cancelNew = async () => {
    this.props.spinner(true)
    const ClientID = await AsyncStorage.getItem(PHONE)
    const { Employee, Service, StartDate } = this.props.data.item
    this.props.cancelGroupAppointmentNew({ ClientID, Employee, Service, StartDate })
  }

  _renderButtonNewClient = () => {
    const { Reserved, loading } = this.props.mail
    if (loading) { return <Spinner /> }
    if (Reserved) {
      return (
        <ReactHoverObserver>
          <ButtonUI title="Отмена" onPress={this._cancelNew} />
        </ReactHoverObserver>
      )
    } else {
      return (
        <ReactHoverObserver>
          <ButtonUI title="Запись" onPress={this._signupNew} />
        </ReactHoverObserver>
      )
    }
  }

  render() {
    const { container } = styles
    const { card } = this.props.data
    const { Employee, Room, Duration, StartDate, EndDate, Service } = this.props.data.item
    const { ClientID } = this.props.clientData
    console.log('ClientID', ClientID)
    return (
      <View> 
        <Header title='Резерв' >
          <View style={container}>
            <Card>
              <CardSection>
                <TextFolder
                  value={Employee.FullName}
                  label="Мастер"
                />
              </CardSection>
              <CardSection>
                <TextFolder
                  value={Service.Title}
                  label="Сервис"
                />
              </CardSection>
              <CardSection>
                <TextFolder
                  value={Room.Title}
                  label="Зал"
                />
              </CardSection>
              <CardSection>
                <TextFolder
                  value={StartDate}
                  label="Начало"
                />
              </CardSection>
              <CardSection>
                <TextFolder
                  value={EndDate}
                  label="Завершение"
                />
              </CardSection>
              <CardSection>
                <TextFolder
                  value={Duration}
                  label="Количество, минут"
                />
              </CardSection>
              { !card && <View style={{ flex: 1, alignSelf: 'center' }}>
                <Text style={{ paddingTop: 10, paddingBottom: 10, fontFamily: constants.FONTLIGHT, color: 'red' }}>Для записи на занятие - купите абонемент в клубе.</Text>
              </View>
              }
              { ClientID &&
                <View style={{ paddingTop: 15, paddingBottom: 20, alignSelf: 'center' }}>
                  {this._renderButton()}
                </View>
              }
              { !ClientID &&
                <View style={{ paddingTop: 15, paddingBottom: 20, alignSelf: 'center' }}>
                  {this._renderButtonNewClient()}
                </View>
              }
            </Card>
          </View>
        </Header> 
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    paddingTop: mobile5 ? 100 : mobile6 ? 120 : tablet ? 180 : desktop ? 180 : '' 
  }
})

const mapStateToProps = state => {
  return { 
    data: state.timeTableData,
    mail: state.mail,
    info: state.me.info,
    clientData: state.me.clientData
  }
}

const Reserve = withRouter(Index)

export default connect(mapStateToProps, { getScheduleItem, setGroupAppointment, setGroupAppointmentNew, cancelGroupAppointmentNew, spinner, cancelGroupAppointment })(Reserve)
