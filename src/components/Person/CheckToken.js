import React from 'react'
import { AsyncStorage, StyleSheet, View } from 'react-native'
import { withRouter } from 'react-router'
import ReactHoverObserver from 'react-hover-observer'
import { PHONE, TOKEN_KEY, FB, responsive } from '../../constants'
import { ButtonUI, Header } from '../common'
import AccountKit from './AccountKit'

const { facebook_app_id, app_secret, version } = FB
const { mobile5, mobile6, tablet, desktop, desktopHD } = responsive

class Index extends React.Component {
  componentDidMount = async () => {
    //await AsyncStorage.removeItem(TOKEN_KEY)
    //await AsyncStorage.removeItem(PHONE)
    const token = await AsyncStorage.getItem(TOKEN_KEY)
    if (token) {
      this.props.history.push('/person')
    } else {
      this._retrieveData()
    }
  }

  getAssessToken = async ({ code, status }) => {
    if (status === 'PARTIALLY_AUTHENTICATED') {
      const res = await fetch(`https://graph.accountkit.com/${version}/access_token?grant_type=authorization_code&code=${code}&access_token=AA|${facebook_app_id}|${app_secret}`, { method: 'GET' })
      const obj = await res.json()
      await AsyncStorage.setItem(TOKEN_KEY, obj.access_token)
      this.confirmAccessToken(obj.access_token)
    } else {
      console.log('getAssessToken error', status)
    }
  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem(TOKEN_KEY)
      if (token !== null) {
        this.confirmAccessToken(token)
      }
    } catch (error) {
      console.log('_retrieveData', error)
    }
  }

  confirmAccessToken = async (access_token) => {
    try {
      const res = await fetch(`https://graph.accountkit.com/${version}/me/?access_token=${access_token}`, { method: 'GET' })
      const obj = await res.json()
      if (obj.application.id === facebook_app_id) {
        this.setState({ visiblebutton: true })
        await AsyncStorage.setItem(TOKEN_KEY, obj.access_token)
        await AsyncStorage.setItem(PHONE, obj.phone.national_number)
        this.props.history.push('/person')
      }
    } catch (error) {
      console.log('confirmAccessToken', error)
      await AsyncStorage.removeItem(TOKEN_KEY)
      await AsyncStorage.removeItem(PHONE)
      this.props.history.push('/checktoken')
    }
  }

  _accountKit = () => {
    return (
      <AccountKit
        appId={facebook_app_id}
        version={version}
        onResponse={(resp) => this.getAssessToken({code: resp.code, status: resp.status })}
        csrf={'21kj3k1jioj23j1kj2kj13'} 
        countryCode={'+7'}
      >
        {p => (
          <View style={{ paddingTop: 15, paddingBottom: 15, alignSelf: 'center' }}>
            <ReactHoverObserver>
              <ButtonUI title='Вход' {...p} />
            </ReactHoverObserver>
          </View>
        )}
      </AccountKit>
    )
  }

  render() {
    const { container } = styles
    return (
      <View>
        <Header title="Личный кабинет">
          <View style={container}>
            {this._accountKit()}
          </View>
        </Header>
      </View>
    ) 
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center', 
    marginTop: mobile5 ? 250 : mobile6 ? 300 : tablet ? 270 : desktop ? 350 : desktopHD ? 400 : ''
  }
})

const CheckToken = withRouter(Index)
export default CheckToken
