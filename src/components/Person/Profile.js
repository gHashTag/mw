import React, { PureComponent } from 'react'
import { View, Text, AsyncStorage } from 'react-native'
import ReactHoverObserver from 'react-hover-observer'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import 'react-tabs/style/react-tabs.css'
import { getClientByPhone, getVisitHistory } from '../../actions'
import { Loading, ButtonUI, TextFolder, Card, CardSection } from '../common'
import { PHONE, TOKEN_KEY, constants } from '../../constants'

class Index extends PureComponent {
  state = { Phone: '', token: '' }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem(TOKEN_KEY)
    if (!token) {
      this.props.history.push('/checktoken')
    } else {
      const Phone = await AsyncStorage.getItem(PHONE)
      this.setState({ Phone, token })
      this.props.getClientByPhone(Phone)
    } 
  }

  goToLogout = async () => {
    await AsyncStorage.removeItem(TOKEN_KEY)
    await AsyncStorage.removeItem(PHONE)
    this.props.history.push('/checktoken')
  }

  render() {
    const { container } = styles
    const { Name, LastName, SecondName, Birthday, Email } = this.props.clientData
    const { Phone } = this.state
    if (!this.state.Phone) { return <Loading /> }
    return (
      <View style={container}>
        { this.props.clientData.Status === 'NotFound' && <View style={{ flex: 1, alignSelf: 'center' }}>
          <Text style={{ paddingBottom: 25, fontFamily: constants.FONTLIGHT, color: 'red' }}>Вы еще не совершали покупок в клубе, поэтому ваша история пуста.</Text>
        </View>
        }
        <Card>
          <CardSection>
            <TextFolder
              value={LastName}
              label="Фамилия"
            />
          </CardSection>
          <CardSection>
            <TextFolder
              value={Name}
              label="Имя"
            />
          </CardSection>
          <CardSection>
            <TextFolder
              value={SecondName}
              label="Отчество"
            />
          </CardSection>
          <CardSection>
            <TextFolder
              value={Birthday}
              label="Дата рождения"
            />
          </CardSection>
          <CardSection>
            <TextFolder
              value={Phone}
              label="Phone +7"
            />
          </CardSection>
          <CardSection>
            <TextFolder
              value={Email}
              label="Email"
            />
          </CardSection>
          <View style={{ paddingTop: 15, paddingBottom: 15, alignSelf: 'center' }}>
            <ReactHoverObserver>
              <ButtonUI title="Выход" onPress={this.goToLogout} />
            </ReactHoverObserver>
          </View>
        </Card>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center'
  }
}

const Person = withRouter(Index)

const mapStateToProps = state => {
  return { 
    clientData: state.me.clientData
  }
}

export default connect(mapStateToProps, { getClientByPhone, getVisitHistory })(Person)

