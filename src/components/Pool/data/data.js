import p0 from '../images/p0.png'
import p1 from '../images/p1.png'
import p2 from '../images/p2.png'
import p3 from '../images/p3.png'
import p4 from '../images/p4.png'

export const data = [
  {
    id: 0,
    name: '0',
    img: p0
  },
  {
    id: 1,
    name: '1',
    img: p1
  },
  {
    id: 2,
    name: '2',
    img: p2
  },
  {
    id: 3,
    name: '3',
    img: p3
  },
  {
    id: 4,
    name: '4',
    img: p4
  }
]
