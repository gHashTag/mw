import React, { PureComponent } from 'react'
import { ScrollView, Dimensions, Image, View, Text } from 'react-native' 
import PriceCard from '../common/PriceCard'
import { data } from './data/data'
import data1 from './data/data1.json'
import data2 from './data/data2.json'
import data3 from './data/data3.json'
import data1baby from './data/data1baby.json'
import data2baby from './data/data2baby.json'
import data3baby from './data/data3baby.json'

const win = Dimensions.get('window')
const w = win.width
const h = win.height + 200
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 991 && w > 415
const desktop = w > 992

const sale = 'Предложение CITY RETREAT CLUB весна / лето 2018. Действует с 9.04 по 31.08 2018 г. включительно. \nДетям до 1.5 лет бесплатно.'
const sale2 = 'Предложение детям до 14 лет, а до 1.5 годовалым детям бесплатно.'

export default class Pool extends PureComponent {
  render() {
    const { container, headerView, header, description } = styles
    return (
      <View>
        { desktop === true &&
          <View>
            <View style={[container, { marginTop: h, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '13vh', paddingBottom: 20 }} scrollEventThrottle={16} style={{ paddingBottom: 20 }} >
                {data.map(({ id, img }) => (
                  <View key={id} style={{ paddingRight: 1 }}>
                    <Image source={img} style={{ height: 300, width: 530 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 50 }]}>
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 50 }]}>
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Парные занятия</Text>
            </View>
            <View style={[container, { marginBottom: 50 }]}>
              {data3.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Детские разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 40 }]}>
              {data1baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale2}</Text>
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Детские клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 50 }]}>
              {data2baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 90 }, headerView]}>
              <Text style={header}>Детские персональные занятия</Text>
            </View>
            <View style={[container, { marginBottom: 300 }]}>
              {data3baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
          </View>
        }

        { tablet === true &&
          <View>
            <View style={[container, { marginTop: h, marginBottom: 15 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '7vh' }} scrollEventThrottle={16} style={{ padding: 20 }} >
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center', paddingRight: 1 }} >
                    <Image source={img} style={{ height: win.width * 0.565, width: win.width }} />
                  </View>
                ))}
              </ScrollView>
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 20 }]}>
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 20 }]}>
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Парные занятия</Text>
            </View>
            <View style={[container, { marginBottom: 20 }]}>
              {data3.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Детские разовые занятия</Text>
            </View>
            <View style={[container, { marginBottom: 20 }]}>
              {data1baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale2}</Text>
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Детские клубные карты</Text>
            </View>
            <View style={[container, { marginBottom: 20 }]}>
              {data2baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
            <View style={[{ marginTop: 60 }, headerView]}>
              <Text style={header}>Детские персональные занятия</Text>
            </View>
            <View style={[container, { marginBottom: 200 }]}>
              {data3baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id}>
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </View>
          </View>
        }

        { mobile6 === true &&
          <View>
            <View style={[container, { marginTop: 60, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '7vh' }} scrollEventThrottle={16}>
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingBottom: 10, paddingRight: 1 }} >
                    <Image source={img} style={{ height: win.width * 0.51, width: win.width - 40 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
            <View style={[{ marginTop: 40, marginBottom: 15 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
            </View>
            <View style={[{ marginTop: 80, marginBottom: 15 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 80, marginBottom: 15 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Парные занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data3.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 80, marginBottom: 15 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Детские разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale2}</Text>
            </View>
            <View style={[{ marginTop: 80, marginBottom: 15 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Детские клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 80, marginBottom: 15 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 29 }}>Детские персональные занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '20%', paddingLeft: '15%' }} scrollEventThrottle={16} style={{ padding: 20, paddingBottom: 200 }} >
              {data3baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
          </View>
        }

        { mobile5 === true &&
          <View>
            <View style={[container, { marginTop: 60, marginBottom: 10 }]}>
              <ScrollView horizontal contentContainerStyle={{ paddingTop: '9vh', paddingBottom: 10 }} scrollEventThrottle={16}>
                {data.map(({ id, img }) => (
                  <View key={id} style={{ flexGrow: 1, justifyContent: 'center', paddingRight: 1 }} >
                    <Image source={img} style={{ height: win.width * 0.49, width: win.width - 40 }} />
                  </View>
                ))}
              </ScrollView>
            </View>
            <View style={[{ marginTop: 10 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale}</Text>
            </View>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Парные занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data3.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Детские разовые занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data1baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#fff', borderColor: '#E7E4E4', h2color: '#4B4B4B', h1color: '#4B4B4B', h3color: '#4B4B4B' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 0 }, headerView]}>
              <Text style={description}>{sale2}</Text>
            </View>
            <View style={[{ marginTop: 80 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Детские клубные карты</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data2baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#4B4B4B', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
            <View style={[{ marginTop: 120 }, headerView]}>
              <Text style={{ textAlign: 'center', alignItems: 'center', fontFamily: 'Museo500', fontSize: 25 }}>Детские персональные занятия</Text>
            </View>
            <ScrollView horizontal contentContainerStyle={{ paddingRight: '12%', paddingLeft: 15, paddingBottom: 100 }} scrollEventThrottle={16} style={{ padding: 20 }} >
              {data3baby.map(({ id, title, status, price, info1, info2, info3, info4 }) => (
                <View key={id} style={{ flexGrow: 1, justifyContent: 'center' }} >
                  <PriceCard 
                    cardinfo={{ id, title, status, price, info1, info2, info3, info4 }}
                    colorcard={{ backgroundColor: '#D1AE6C', borderColor: '#D1AE6C', h2color: '#fff', h1color: '#fff', h3color: '#fff' }} 
                  />
                </View>
              ))}
            </ScrollView>
          </View>
        }
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
    marginBottom: 40
  },
  headerView: {
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    alignItems: 'center'
  },
  header: {
    alignItems: 'center',
    fontFamily: 'Museo500', 
    fontSize: 35 
  },
  description: {
    alignItems: 'center',
    textAlign: 'center',
    fontFamily: 'CirceLight', 
    marginLeft: 20,
    marginRight: 20,
    fontSize: 15 
  }
}
