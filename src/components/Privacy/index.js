import React, { PureComponent } from 'react'
import { View, Text, Dimensions } from 'react-native'
import AOS from 'aos'
import { constants } from '../../constants'
import data from './data.json'
import { Header } from '../common'
import '../aos.css'
 
const win = Dimensions.get('window')
const w = win.width
const mobile5 = w > 315 && w < 341
const mobile6 = w > 342 && w < 415
const tablet = w < 1023 && w > 415
const desktop = w >= 1024 

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: mobile5 ? 130 : mobile6 ? 130 : tablet ? 200 : desktop ? 200 : '', //eslint-disable-line
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 200,
    alignItems: 'center'
  },
  h1: {
    fontFamily: constants.FONTHEADER,
    fontSize: mobile5 ? 17 : mobile6 ? 22 : tablet ? 30 : desktop ? 30 : '', //eslint-disable-line
    paddingLeft: 15,
    paddingRight: 15,
    fontStyle: 'normal',
    lineHeight: 'normal',
    textAlign: 'center',
    color: '#000' 
  },
  h2: {
    width: (win.width / 2) + 68,
    marginTop: 20,
    height: 246,
    opacity: 0.73,
    fontFamily: 'CirceLight',
    fontSize: mobile5 ? 12 : mobile6 ? 15 : tablet ? 16 : desktop ? 16 : '', //eslint-disable-line
    fontStyle: 'normal',
    lineHeight: mobile5 ? 12 : mobile6 ? 14 : tablet ? 16 : desktop ? 18 : '', //eslint-disable-line
    textAlign: 'left',
    color: constants.FONTCOLOR 
  }
}

AOS.init()

export default class Privacy extends PureComponent {
  render() {
    const { container, h1, h2 } = styles
    const { title, title1, title2, title3, title4, title5, title6, title7, title8, title9, title10, title11, title12, title13, info, info1, info2, info3, info4, info5, info6, info7, info8, info9, info10, info11, info12, info13, date } = data
    return (
      <View>
        <Header title='Соглашение' />
        <View style={container}>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title}</Text>
          <View style={{ height: mobile5 ? 400 : mobile6 ? 500 : tablet ? 400 : desktop ? 330 : ''}}> 
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title1}</Text>
          <View style={{ height: mobile5 ? 1000 : mobile6 ? 1250 : tablet ? 1450 : desktop ? 700 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info1}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title2}</Text>
          <View style={{ height: mobile5 ? 1000 : mobile6 ? 1250 : tablet ? 950 : desktop ? 700 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info2}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title3}</Text>
          <View style={{ height: mobile5 ? 600 : mobile6 ? 830 : tablet ? 600 : desktop ? 450 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info3}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title4}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 250 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info4}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title5}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 200 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info5}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title6}</Text>
          <View style={{ height: mobile5 ? 230 : mobile6 ? 280 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info6}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title7}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 230 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info7}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title8}</Text>
          <View style={{ height: mobile5 ? 270 : mobile6 ? 350 : tablet ? 280 : desktop ? 200 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info8}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title9}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 280 : tablet ? 230 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info9}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title10}</Text>
          <View style={{ height: mobile5 ? 150 : mobile6 ? 200 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info10}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title11}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 250 : tablet ? 200 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info11}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title12}</Text>
          <View style={{ height: mobile5 ? 130 : mobile6 ? 200 : tablet ? 150 : desktop ? 150 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info12}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='2000' data-aos-delay='200' style={h1}>{title13}</Text>
          <View style={{ height: mobile5 ? 200 : mobile6 ? 200 : tablet ? 250 : desktop ? 200 : '' }}>
            <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{info13}</Text>
          </View>

          <Text data-aos='fade-up' data-aos-duration='3000' data-aos-delay='100' className='museo' style={h2}>{date}</Text>
        </View>
      </View>
    )
  }
}
