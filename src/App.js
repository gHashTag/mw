import React, { Component } from 'react'
import { Dimensions, Image, View, Text } from 'react-native' // eslint-disable-line
import { BrowserRouter as Router, NavLink } from 'react-router-dom'
import { stack as Menu } from 'react-burger-menu'
import WebFont from 'webfontloader'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faUniversity from '@fortawesome/fontawesome-free-solid/faUniversity'
import faStar from '@fortawesome/fontawesome-free-solid/faStar'
import faKey from '@fortawesome/fontawesome-free-solid/faKey'
import faImage from '@fortawesome/fontawesome-free-solid/faImage'
import faPlane from '@fortawesome/fontawesome-free-solid/faPlane'
import faGraduationCap from '@fortawesome/fontawesome-free-solid/faGraduationCap'
import faGlobe from '@fortawesome/fontawesome-free-solid/faGlobe'
import faLeaf from '@fortawesome/fontawesome-free-solid/faLeaf'
import faDiagnoses from '@fortawesome/fontawesome-free-solid/faDiagnoses'
import faCalendarAlt from '@fortawesome/fontawesome-free-solid/faCalendarAlt'
import faCreditCard from '@fortawesome/fontawesome-free-solid/faCreditCard'
import './icon.css'
import Burger from './burger.svg'
import Routes from './routes.js'
import './fonts.css'

const WebFontConfig = {
  custom: {
    families: ['Museo500', 'Carlito', 'Arnamu', 'Odyssey', 'CirceLight', 'CirceExtraLight']
  },
  google: {
    families: ['Roboto']
  }
}
WebFont.load(WebFontConfig)

const platform = (x) => {
  try {
    const w = window.innerWidth
    if (w <= 320) {
      return x + 3 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x * 3.7 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x * 3.9 // 'iphone6+'
    } else if (w > 414) {
      return x * 3.9 // 'web'
    }
  } catch (e) {
    throw e
  }
}

const menuY = (x) => {
  try {
    const w = window.innerWidth
    if (w <= 320) {
      return x + 2 //'iphone5'
    } else if (w <= 375 && w > 321) {
      return x + 3 // 'iphone6'
    } else if (w <= 414 && w > 375) {
      return x + 3 //'iphone6+'
    } else if (w > 414) {
      return x + 20// 'web'
    }
  } catch (e) {
    throw e
  }
}

const styles = {
  bmBurgerButton: {
    position: 'fixed',
    width: '40px',
    height: '53px',
    left: platform(5) + 9,
    top: menuY(1) 
  },
  bmCrossButton: {
    position: 'absolute',
    right: '10px',
    height: '24px',
    width: '24px'
  },
  bmCross: {
    background: '#fff'
  },
  bmMenu: {
    background: 'rgba(74, 74, 74, 0.70)',
    fontWeight: '500',
    fontSize: '1.15em'
  },
  bmMorphShape: {
    fill: '#373a47'
  },
  bmItemList: {
    color: '#fff',
    marginTop: 80,
    marginLeft: 90
  },
  bmOverlay: {
    background: 'rgba(0, 0, 0, 0.6)'
  },
  navLink: {
    color: '#fff', 
    fontSize: 17, 
    fontFamily: 'CirceLight',
    paddingTop: 2,
    paddingLeft: 15
  },
  icons: {
    width: 20,
    paddingTop: 2
  },
  activeStyle: {
    color: '#D6B784' 
  },
  link: {
    textDecoration: 'none'
  }
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menuOpen: false
    }
  }

  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen })  
  }

  closeMenu() {
    this.setState({ menuOpen: false })
  }
  toggleMenu() {
    this.setState({ menuOpen: !this.state.menuOpen })
  }

  render() {
    const { navLink, activeStyle, link, icons } = styles

    return (
      <Router>
        <div>
          <Menu
            isOpen={this.state.menuOpen}
            onStateChange={(state) => this.handleStateChange(state)}
            styles={styles}
            customBurgerIcon={!this.state.menuOpen ? <Image source={Burger} /> : false} 
          >
            <div style={{ marginTop: 16 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faUniversity} />
                  <Text style={navLink}>Главная</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/events/saying' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faStar} />
                  <Text style={navLink}>События</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/gallery/0' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faImage} />
                  <Text style={navLink}>Галерея</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/master/feliks-pak' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faGraduationCap} />
                  <Text style={navLink}>Мастера</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/timetable' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faCalendarAlt} />
                  <Text style={navLink}>Расписание</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/bar' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faLeaf} />
                  <Text style={navLink}>Эко бар</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/pool' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faDiagnoses} />
                  <Text style={navLink}>Бассейн</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/travels/top6' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faPlane} />
                  <Text style={navLink}>Путешествия</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/price' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faCreditCard} />
                  <Text style={navLink}>Стоимость</Text>
                </View>
              </NavLink>
            </div>
            <div style={{ marginTop: 7 }}> <NavLink style={link} exact onClick={() => this.closeMenu()} to='/checktoken' activeStyle={activeStyle} >
              <View style={{ flexDirection: 'row' }}>
                <FontAwesomeIcon style={icons} icon={faKey} />
                <Text style={navLink}>Личный кабинет</Text>
              </View>
            </NavLink>
            </div>
            <div style={{ marginTop: 7 }}>
              <NavLink style={link} exact onClick={() => this.closeMenu()} to='/contacts' activeStyle={activeStyle} >
                <View style={{ flexDirection: 'row' }}>
                  <FontAwesomeIcon style={icons} icon={faGlobe} />
                  <Text style={navLink}>Контакты</Text>
                </View>
              </NavLink>
            </div>
          </Menu>
          <Routes />
        </div>
      </Router>
    )
  }
}

export default App
