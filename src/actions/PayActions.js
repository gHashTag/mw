import shortid from 'shortid'
import sha1 from 'js-sha1'
import { constants } from '../constants'

export const getPayRequest = ({ order_desc, amount }) => {
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'PAY_REQUEST', payload: success })
      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_PAY_REQUEST', error })
      return error
    }
    try {
      const payment_key = 'kQNbxEa5PE3Ww0LCrOH4t7a70fQjzO6A'
      const currency = 'RUB'
      const merchant_id = '1410804'
      const order_id = shortid.generate()
      const body = {
        request: {
          order_id,
          order_desc,
          currency,
          amount,
          merchant_id,
          signature: sha1(`${payment_key}|${amount}|${currency}|${merchant_id}|${order_desc}|${order_id}`)
        }
      }
      const res = await fetch(constants.URLFONDY, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const success = await res.json()
      return onSuccess(success)
    } catch (error) {
      console.log('error getPayRequest', error)
      return onError(error)
    }
  }
}
