import { constants } from '../constants'

export const getMe = (info) => {
  try {
    return {
      type: 'GET_ME_INFO',
      info
    } 
  } catch (e) {
    return {
      type: 'GET_ME_INFO_ERROR',
      info
    } 
  }
}

export const getClientByPhone = (info) => {
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'CLIENT_FETCHED', payload: success })
      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_CLIENT_FETCHED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'getClientByPhone',
        Parameters: {
          Phone: `7${info}`
        } 
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      return onSuccess(success)
    } catch (error) {
      console.log('error getClientByPhone', error)
      return onError(error)
    }
  }
}

export const getVisitHistory = (info) => {
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'GET_CLIENT_VISIT', payload: success })
      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_CLIENT_VISITED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'getVisitHistory',
        Parameters: {
          ClientID: info,
          ServiceID: null,
          Count: 50,
          CountNumber: 1,
          Timestamp: null
        } 
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      return onSuccess(success)
    } catch (error) {
      console.log('error getVisitHistory', error)
      return onError(error)
    }
  }
}

export const logOut = (dispatch) => {
  dispatch({ type: 'LOGOUT' })
}

