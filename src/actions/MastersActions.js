import { find, filter } from 'lodash'
import { constants } from '../constants'
import { data } from './data.js'

export const fetchMasters = ({ apiMeth, params }) => {  
  //console.log('thunk', apiMeth, params)
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'MASTERS_FETCHED', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_MASTERS_FETCHED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: apiMeth,
        Parameters: params 
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = filter(Parameters, { Position: { Title: 'Преподаватель' } }) 
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const fetchMaster = ({ apiMeth, ID }) => {  
  //console.log('thunk', apiMeth, ID)
  return async dispatch => {
    function onSuccess(merge) {
      dispatch({ type: 'MASTER_FETCHED', payload: merge })

      return merge 
    }
    function onError(error) {
      dispatch({ type: 'ERROR_MASTER_FETCHED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: apiMeth,
        Parameters: {
          TrainerID: ID
        } 
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      const newData = find(data, { ID: success.ID })
      const merge = { success, newData }
      return onSuccess(merge)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}
