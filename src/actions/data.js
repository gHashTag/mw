import m0 from './masters/m0.png'
import m1 from './masters/m1.png'
//import m2 from './masters/m2.png'
import m3 from './masters/m3.png'
import m4 from './masters/m4.png'
import m5 from './masters/m5.png'
import m6 from './masters/m6.png'
import m7 from './masters/m7.png'
import m8 from './masters/m8.png'
import m9 from './masters/m9.png'
import m10 from './masters/m10.png'
import m11 from './masters/m11.png'
import m12 from './masters/m12.png'
import m13 from './masters/m13.png'
import m14 from './masters/m14.png'
import m15 from './masters/m15.png'
import m16 from './masters/m16.png'
import m17 from './masters/m17.png'
//import m18 from './masters/m18.png'
import m19 from './masters/m19.png'
import m20 from './masters/m20.png'
//import m21 from './masters/m21.png'
//import m22 from './masters/m22.png'
import m23 from './masters/m23.png'
import m24 from './masters/m24.png'
import m25 from './masters/m25.png'
import m26 from './masters/m26.png'
import m27 from './masters/m27.png'
import m28 from './masters/m28.png'
import m29 from './masters/m29.png'

export const data = [
  {
    ID: '5419a1d0-48a4-11e8-8119-0050568b1a3a',
    Name: 'Дубнель Юлия',
    Title: 'Хатха-йога',
    Photo: m0
  },
  {
    ID: '2992c767-48a9-11e8-8119-0050568b1a3a',
    Name: 'Рудницкая Ирина',
    Title: 'Телесные практики',
    Photo: m24
  },
  {
    ID: 'b63ef2c8-48a9-11e8-8119-0050568b1a3a',
    Name: 'Фатеева Элина',
    Title: 'Йога критического выравнивания',
    Photo: m20
  },
  {
    ID: '00983d9a-48aa-11e8-8119-0050568b1a3a',
    Name: 'Авдеева Елена',
    Title: 'Йога критического выравнивания',
    Photo: m3
  },
  {
    ID: '539d069e-48aa-11e8-8119-0050568b1a3a',
    Name: 'Мирзабекян Александр',
    Title: 'Кундалини Йога',
    Photo: m27
  },
  {
    ID: '7ec97ac2-48aa-11e8-8119-0050568b1a3a',
    Name: 'Семичева Ольга',
    Title: 'Аштанга-Виньяса',
    Photo: m14
  },
  {
    ID: 'd6b8dee2-49ff-11e8-8119-0050568b1a3a',
    Name: 'Зяблицкая Александра',
    Title: 'Айенгара',
    Photo: m5
  },
  {
    ID: 'e677355a-4a16-11e8-8119-0050568b1a3a',
    Name: 'Пак Феликс',
    Title: 'Сила&Баланс',
    Photo: m12
  },
  {
    ID: '1d32d3f0-4ed3-11e8-8119-0050568b1a3a',
    Name: 'Байков Алексей',
    Title: 'Крийя-Тантра-Йога',
    Photo: m19
  },
  {
    ID: '6f17ad8c-4ed3-11e8-8119-0050568b1a3a',
    Name: 'Левченко Евгений',
    Title: 'Хатха йога',
    Photo: m4
  },
  {
    ID: 'a90557f6-4ed3-11e8-8119-0050568b1a3a',
    Name: 'Струц Юлия',
    Title: 'Стрейч',
    Photo: m6
  },
  {
    ID: '68af2757-4ed5-11e8-8119-0050568b1a3a',
    Name: 'Чулибаев Фарзон',
    Title: 'Функциональная тренировка',
    Photo: m23
  },
  {
    ID: 'd8bd324b-4ed5-11e8-8119-0050568b1a3a',
    Name: 'Фёдоров Дмитрий',
    Title: 'System 432',
    Photo: m1
  },
  {
    ID: 'fe54a0d9-4ed5-11e8-8119-0050568b1a3a',
    Name: 'Ахмадеев Даниэль',
    Title: 'Практика Осознанности',
    Photo: m25
  },
  {
    ID: '6d93c84d-4ed6-11e8-8119-0050568b1a3a',
    Name: 'Казанцева Ульяна',
    Title: 'Yoga 23',
    Photo: m29
  },
  {
    ID: 'bf22a9fa-4ed6-11e8-8119-0050568b1a3a',
    Name: 'Гатина Светлана',
    Title: 'Yoga 23',
    Photo: ''
  },
  {
    ID: '21fc467c-4ed7-11e8-8119-0050568b1a3a',
    Name: 'Ковалёв Алексей',
    Title: 'Телесная терапия',
    Photo: m13
  },
  {
    ID: '7dce038b-4ed7-11e8-8119-0050568b1a3a',
    Name: 'Панцалашвили Лия',
    Title: 'Рукопашный бой',
    Photo: m7
  },
  {
    ID: 'ce28d055-4ed7-11e8-8119-0050568b1a3a',
    Name: 'Егоров Филипп',
    Title: '',
    Photo: m10
  },
  {
    ID: '368e52e1-4ed8-11e8-8119-0050568b1a3a',
    Name: 'Мещерякова Евгения',
    Title: '',
    Photo: m9
  },
  {
    ID: '9ebc2f71-4ed8-11e8-8119-0050568b1a3a',
    Name: 'Мизилин Виктор',
    Title: '',
    Photo: m8
  },
  {
    ID: '5b6fe51c-4eeb-11e8-8119-0050568b1a3a',
    Name: 'Ревтов Сергей',
    Title: '',
    Photo: m28
  },
  {
    ID: '32be1b27-4eec-11e8-8119-0050568b1a3a',
    Name: 'Высоцкий Александр',
    Title: 'Cпортивная медицина',
    Photo: m15
  },
  {
    ID: 'a619bc45-4eed-11e8-8119-0050568b1a3a',
    Name: 'Хапкова Екатерина',
    Title: '',
    Photo: m11
  },
  {
    ID: '6b0924f2-4eee-11e8-8119-0050568b1a3a',
    Name: 'Митус Татьяна',
    Title: 'Кундалини йога',
    Photo: m16
  },
  {
    ID: '3cb354b5-4eef-11e8-8119-0050568b1a3a',
    Name: 'Хуснуллин Камиль',
    Title: 'Yoga 23',
    Photo: m26
  },
  {
    ID: 'a5d9fa2e-4ed5-11e8-8119-0050568b1a3a',
    Name: 'Лазарева Екатерина',
    Title: 'Body Ballet',
    Photo: m17
  }
]
