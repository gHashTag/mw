import { constants } from '../constants'

export const clearStore = () => {
  return {
    type: 'CLEAR_STORE'
  }
}

export const getSchedule = ({ StartDate, EndDate }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'SCHEDULE_FETCHED', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_SCHEDULE_FETCHED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'getSchedule',
        Parameters: { 
          StartDate: StartDate.format('YYYY-MM-DD HH:mm'), 
          EndDate: EndDate.format('YYYY-MM-DD HH:mm') 
        }
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const getScheduleItem = ({ ClientID, AppointmentID }) => {  
  //console.log('getScheduleItem', ClientID)
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'SCHEDULE_ITEM_FETCHED', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_SCHEDULE_ITEM_FETCHED', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'getScheduleItem',
        Parameters: { 
          ClientID,
          AppointmentID,
          Reserve: false
        }
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const setGroupAppointment = ({ ClientID, AppointmentID }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'SET_GROUP_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_SET_GROUP_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'SetGroupAppointment',
        Parameters: { 
          ClientID,
          AppointmentID,
          Reserve: false
        }
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const cancelGroupAppointment = ({ ClientID, AppointmentID }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'CANCEL_GROUP_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_CANCEL_GROUP_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        Request_id: '7a6b8835-b8db-4413-a9f4-115c1ebe7b7c',
        ClubId: '5f6c4a6b-3bed-11e8-8115-000c297e508b',
        Method: 'CancelGroupAppointment',
        Parameters: { 
          ClientID,
          AppointmentID,
          Reserve: false
        }
      }
      const res = await fetch(constants.URL, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Basic ' + btoa('raoffonom@icloud.com:HelloWorld'), // eslint-disable-line
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const obj = await res.json()
      const { Parameters } = obj 
      const success = Parameters 
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const setGroupAppointmentNew = ({ ClientID, Employee, Service, StartDate }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'SIGN_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_SIGN_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        to: 'info@cityretreat.ru',
        subject: `Новый клиент ${ClientID}`,
        text: `${Employee.FullName} | ${Service.Title} | ${StartDate}`
      }
      const res = await fetch(constants.MAILGUN, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const success = await res.text()
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const cancelGroupAppointmentNew = ({ ClientID, Employee, Service, StartDate }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'CANCEL_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_CANCEL_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        to: 'info@cityretreat.ru',
        subject: `Отмена клиент ${ClientID}`,
        text: `${Employee.FullName} | ${Service.Title} | ${StartDate}`
      }
      const res = await fetch(constants.MAILGUN, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const success = await res.text()
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const spinner = (item) => {
  return {
    type: 'SPINNER',
    payload: item
  }
}

