import { constants } from '../constants'

export const setGroupAppointmentNew = ({ ClientID, Employee, Service, StartDate }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'SIGN_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_SIGN_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        to: 'info@cityretreat.ru',
        subject: `Новый клиент ${ClientID}`,
        text: `${Employee.FullName} | ${Service.Title} | ${StartDate}`
      }
      const res = await fetch(constants.MAILGUN, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const success = await res.text()
      //console.log('success', success)
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}

export const cancelGroupAppointmentNew = ({ ClientID, Employee, Service, StartDate }) => {  
  return async dispatch => {
    function onSuccess(success) {
      dispatch({ type: 'CANCEL_APPOINTMENT', payload: success })

      return success
    }
    function onError(error) {
      dispatch({ type: 'ERROR_CANCEL_APPOINTMENT', error })
      return error
    }
    try {
      const body = {
        to: 'info@cityretreat.ru',
        subject: `Отмена клиент ${ClientID}`,
        text: `${Employee.FullName} | ${Service.Title} | ${StartDate}`
      }
      const res = await fetch(constants.MAILGUN, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      const success = await res.text()
      return onSuccess(success)
    } catch (error) {
      console.log('error', error)
      return onError(error)
    }
  }
}
