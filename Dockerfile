FROM nginx:alpine
COPY ./nginx /etc/nginx/conf.d/
COPY build/ /usr/share/nginx/html
