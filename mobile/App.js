/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react'
import { WebView } from 'react-native'

export default class App extends Component {
  render() {
    return (
      <WebView
        originWhitelist={['*']}
        source={{uri: 'https://www.cityretreat.ru'}}
        style={{marginTop: 20}}
      />
    )
  }
}
